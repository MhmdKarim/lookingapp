//import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:lookin/Views/Filter/filter_page.dart';
import 'package:lookin/Views/More/About_us_page.dart';
import 'package:lookin/Views/More/change_language_page.dart';
import 'package:lookin/Views/More/contact_us_page.dart';
import 'package:lookin/Views/More/edit_profile_page.dart';
import 'package:lookin/Views/More/messages_page.dart';
import 'package:lookin/Views/More/privacy_policy_page.dart';
import 'package:lookin/Views/More/terms_conditions_page.dart';
import 'package:lookin/Views/StartPages/language_page.dart';
import 'package:lookin/Views/Details/details_page.dart';
import 'package:lookin/Views/chat_page.dart';
import 'package:lookin/Views/divisions_page.dart';
import 'package:lookin/Views/favourite_page.dart';
import 'Views/Details/ratings_page.dart';
import 'Views/StartPages/splash_screen.dart';
import 'Views/search_page.dart';
import 'Widgets/dashboard.dart';
import 'PreDefined/localization.dart';
import 'PreDefined/app_constants.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(
    MyApp()
  // DevicePreview(builder: (context) => MyApp(),enabled: !kReleaseMode,)
    
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale;
  //Initial value before any selection is made

  final routes = <String, WidgetBuilder>{
    Dashboard.tag: (context) => Dashboard(),
    DetailsPage.tag: (context) => DetailsPage(),
    LanguagePage.tag: (context) => LanguagePage(),
    AboutUsPage.tag: (context) => AboutUsPage(),
    ChangeLanguagePage.tag: (context) => ChangeLanguagePage(),
    ContactUsPage.tag: (context) => ContactUsPage(),
    MessagesPage.tag: (context) => MessagesPage(),
    PrivacyPolicyPage.tag: (context) => PrivacyPolicyPage(),
    TermsConditionsPage.tag: (context) => TermsConditionsPage(),
    EditProfilePage.tag: (context) => EditProfilePage(),
    FilterPage.tag: (context) => FilterPage(),
    FavouritePage.tag: (context) => FavouritePage(),
    SearchPage.tag: (context) => SearchPage(),
    RatingsPage.tag: (context) => RatingsPage(),
    SplashScreenPage.tag: (context) => SplashScreenPage(),
    ChatPage.tag: (context) => ChatPage(),
    DivisionsPage.tag: (context) => DivisionsPage(),
  };

  @override
  void initState() {
    super.initState();
    _locale = appLanguageId == 1 ? Locale('en') : Locale('ar');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     // locale: DevicePreview.of(context).locale, // <--- Add the locale
     // builder: DevicePreview.appBuilder,  // <--- Add the builder
      title: 'lookin',
      theme: ThemeData(
        primaryColor: primaryColor,
        accentColor: accentColor,
        fontFamily: 'Almarai',
        cursorColor: primaryColor,
        textSelectionColor: primaryColor,
        textSelectionHandleColor: primaryColor,
      ),
      debugShowCheckedModeBanner: false,
      home: SplashScreenPage(),
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', ''),
        const Locale('ar', ''),
      ],
      routes: routes,
      locale: _locale,
    );
  }
}
