import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  //Sign out
  signOut() {
    FirebaseAuth.instance.signOut();
  }

  //SignIn
  signIn(AuthCredential authCreds) async {
    FirebaseAuth.instance.signInWithCredential(authCreds);
    AuthResult result =
        await FirebaseAuth.instance.signInWithCredential(authCreds);
    FirebaseUser user = result.user;
  }

  signInWithOTP(smsCode, verId) {
    AuthCredential authCreds = PhoneAuthProvider.getCredential(
        verificationId: verId, smsCode: smsCode);
    signIn(authCreds);
    print('signInWithOTP');
  }
}
