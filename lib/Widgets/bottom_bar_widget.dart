import 'package:flutter/material.dart';
import 'package:lookin/PreDefined/localization.dart';
import 'package:lookin/Widgets/text_widgets.dart';
import '../PreDefined/app_constants.dart';

typedef BottomTabsCallback = void Function(int index);

class AppBottomBar extends StatefulWidget {
  final BottomTabsCallback onBottomTabSelect;
  final int currentbarIndex;

  AppBottomBar({
    @required this.onBottomTabSelect,
    @required this.currentbarIndex,
  });

  @override
  _AppBottomBarState createState() => _AppBottomBarState();
}

class _AppBottomBarState extends State<AppBottomBar> {
  AppLocalizations appLocalizations;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Container(
        child: BottomNavigationBar(
          //backgroundColor: Theme.of(context).primaryColor,
          type: BottomNavigationBarType.fixed,
          onTap: widget.onBottomTabSelect,
          currentIndex: widget.currentbarIndex,
          //fixedColor: Theme.of(context).accentColor,
          items: [
            BottomNavigationBarItem(
              activeIcon: Image.asset(
                'assets/home.png',
                height: 30,
              ),
              title: MyText.sText(
                context: context,
                text: 'Home',
              ),
              icon: Image.asset(
                'assets/home-in.png',
                height: 30,
              ),
            ),
            // BottomNavigationBarItem(
            //   activeIcon: Image.asset(
            //     'assets/categories.png',
            //     height: 30,
            //   ),
            //   title: MyText.sText(
            //     context: context,
            //     text: 'Categories',
            //   ),
            //   icon: Image.asset(
            //     'assets/categories-in.png',
            //     height: 30,
            //   ),
            // ),
            BottomNavigationBarItem(
              activeIcon: Image.asset(
                'assets/requests.png',
                height: 30,
              ),
              title: MyText.sText(
                context: context,
                text: 'Requests',
              ),
              icon: Image.asset(
                'assets/requests-in.png',
                height: 30,
              ),
            ),
            BottomNavigationBarItem(
              activeIcon: CircleAvatar(
                radius: 16,
                backgroundColor: primaryColor,
                child: Icon(
                  Icons.star,
                  size: 25,
                  color: canvusColor,
                ),
              ),
              title: MyText.sText(
                context: context,
                text: 'Favourites',
              ),
              icon: CircleAvatar(
                radius: 16,
                backgroundColor: accentColor,
                child: Icon(
                  Icons.star,
                  size: 25,
                  color: canvusColor,
                ),
              ),
            ),

            BottomNavigationBarItem(
              activeIcon: Image.asset(
                'assets/more.png',
                height: 30,
              ),
              title: MyText.sText(
                context: context,
                text: 'More',
              ),
              icon: Image.asset(
                'assets/more-in.png',
                height: 30,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
