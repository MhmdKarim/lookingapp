  import 'package:flutter/material.dart';
class ShowSheet{
static void showSheet(BuildContext ctx, Widget sheet) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      context: ctx,
      builder: (context) {
        return Wrap(children: <Widget>[
          sheet,
        ]);
      },
    );
  }}