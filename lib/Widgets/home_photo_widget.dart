import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Models/home_model.dart';
import 'package:lookin/PreDefined/app_constants.dart';

class HomePhotoWidget extends StatefulWidget {
  final RestaurantsData data;
  const HomePhotoWidget(this.data);
  @override
  _PhotoWidgetState createState() => _PhotoWidgetState();
}

class _PhotoWidgetState extends State<HomePhotoWidget> {
  String photo;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 250,
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(10),
                height: 200,
                width: double.infinity,
                child: Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: widget.data.cover != null
                        ? CachedNetworkImage(
                            imageUrl: widget.data.cover,
                            fit: BoxFit.cover,
                            errorWidget: (context, url, error) => Image.asset(
                              'assets/logo1.png',
                              fit: BoxFit.fitWidth,
                            ),
                          )
                        : Image.asset(
                            'assets/logo1.png',
                            fit: BoxFit.fitWidth,
                          ),
                  ),
                ),
              ),
              constUserLoginModel.id == 0
                  ? Container()
                  : Positioned.directional(
                      textDirection:
                          isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                      end: 30,
                      top: 20,
                      child: InkWell(
                        onTap: () {
                          print('star tapped');
                          widget.data.isLike
                              ? deleteFromFavourite(widget.data.id)
                              : addToFavourite(widget.data.id);
                        },
                        child: CircleAvatar(
                          radius: 25,
                          backgroundColor: primaryColor,
                          child: CircleAvatar(
                            backgroundImage: AssetImage(
                                widget.data.isLike //||widget.data.isLike
                                    ? 'assets/star.png'
                                    : 'assets/star-in.png'),
                            radius: 16,
                            backgroundColor: primaryColor,
                          ),
                        ),
                      ),
                    ),
              Positioned.directional(
                textDirection:
                    isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                start: 30,
                bottom: 0,
                child: CircleAvatar(
                  radius: 40,
                  backgroundImage: NetworkImage(
                    widget.data.image,
                  ),
                  backgroundColor: Colors.transparent,
                ),
              ),
              Positioned.directional(
                  textDirection:
                      isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                  start: 140,
                  bottom: 0,
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(widget.data.name),
                        Text(widget.data.subCategory.name),
                      ],
                    ),
                  ))
            ],
          ),
        ),
        SizedBox(height: 10)
      ],
    );
  }

  addToFavourite(int storeId) async {
    Map<String, dynamic> jsonMap = {"main_store_id": storeId};

    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'favorites',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      if (mounted) {
        setState(() {
          widget.data.isLike = true;
          // _restaurantsData = homeModel.data;
          //_isLoading = false;
        });
      }
    } else {
      //_isLoading = true;
    }
  }

  deleteFromFavourite(int storeId) async {
    // Map<String, dynamic> jsonMap = {"main_store_id": storeId};

    // String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.delete(
      'favorites/$storeId',
      // jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      if (mounted) {
        setState(() {
          widget.data.isLike = false;
          // _restaurantsData = homeModel.data;
          //_isLoading = false;
        });
      }
    } else {
      //_isLoading = true;
    }
  }
}
