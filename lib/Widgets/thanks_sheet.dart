import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lookin/Widgets/standard_button_widget.dart';
import '../PreDefined/app_constants.dart';
import 'text_widgets.dart';

class ThanksSheet extends StatelessWidget {
  final String title;
  final String message;
  final String buttonTitle;
  final Function function;

  ThanksSheet({this.title, this.message, this.function, this.buttonTitle});

  @override
  Widget build(BuildContext context) {
    // AppLocalizations appLocalizations = AppLocalizations.of(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.bottom + 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Container(
                    height: 5,
                    width: 50,
                    color: accentColor,
                  ),
                ),
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  height: 120,
                  width: 120,
                  // margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/success.png')),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: MyText.mTextBold(context: context, text: title),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: MyText.mText(
                  context: context,
                  text: message,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: Btn.button(
                  context: context,
                  title: buttonTitle,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
