import 'package:flutter/material.dart';
import 'package:lookin/PreDefined/localization.dart';
import '../PreDefined/app_constants.dart';

class MyText {

   static  Widget xsText({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: xsFont),
    );
  }
   static Widget xsTextNL({BuildContext context, String text, Color color}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: xsFont),
    );
  }
  static Widget sText({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: sFont),
    );
  }
    static Widget sTextBold({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: sFont,fontWeight: FontWeight.bold),
    );
  }
  static Widget sTextNL({BuildContext context, String text, Color color}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: sFont),
    );
  }
    static Widget sTextNLBold({BuildContext context, String text, Color color}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: sFont,fontWeight: FontWeight.bold),
    );
  }
  static Widget mText({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: mFont),
    );
  }

  static Widget mTextBold({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(
          color: color, fontSize: mFont, fontWeight: FontWeight.bold),
    );
  }

  static Widget mTextNL({BuildContext context, String text, Color color}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: mFont),
    );
  }

  static Widget mTextNLBold({BuildContext context, String text, Color color}) {
    return Text(
      text,
      style: TextStyle(
          color: color, fontSize: mFont, fontWeight: FontWeight.bold),
    );
  }
  static Widget lText({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: lFont),
    );
  }

  static Widget lTextBold({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(
          color: color, fontSize: lFont, fontWeight: FontWeight.bold),
    );
  }
  static Widget lTextNL({BuildContext context, String text, Color color}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: lFont),
    );
  }

  static Widget lTextNLBold({BuildContext context, String text, Color color}) {
    return Text(
      text,
      style: TextStyle(
          color: color, fontSize: lFont, fontWeight: FontWeight.bold),
    );
  }
  static Widget xlText({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: xlFont),
    );
  }
  static Widget xlTextBold({BuildContext context, String text, Color color}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: xlFont, fontWeight: FontWeight.bold),
    );
  }
  static Widget xlTextNL({BuildContext context, String text, Color color}) {
    return Text(
       text,
      style: TextStyle(color: color, fontSize: xlFont),
    );
  }

  static Widget xlTextNLBold({BuildContext context, String text, Color color}) {
    return Text(
       text,
      style: TextStyle(color: color, fontSize: xlFont,fontWeight: FontWeight.bold),
    );
  }

}
