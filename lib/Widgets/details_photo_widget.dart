import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Models/details_model.dart';
import 'package:lookin/PreDefined/app_constants.dart';

class DetailsPhotoWidget extends StatefulWidget {
  final DetailsModel detailsModel;

  const DetailsPhotoWidget(this.detailsModel);

  @override
  _PhotoWidgetState createState() => _PhotoWidgetState();
}

class _PhotoWidgetState extends State<DetailsPhotoWidget> {
  String photo;
  // bool isFavourite = false;

  @override
  void initState() {
    print(widget.detailsModel.isLike);
     print( widget.detailsModel.slider.first);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 250,
          //width: double.infinity,
          child: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(10),
                height: 200,
                width: double.infinity,
                child: Swiper(
                  itemBuilder: (BuildContext context, int index) {
                    return ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: widget.detailsModel.slider[index] != null
                          ? CachedNetworkImage(
                              imageUrl: widget.detailsModel.slider[index],
                              fit: BoxFit.cover,
                              errorWidget: (context, url, error) => Image.asset(
                                'assets/logo1.png',
                                fit: BoxFit.fitWidth,
                              ),
                            )
                          : Image.asset(
                              'assets/logo1.png',
                              fit: BoxFit.fitWidth,
                            ),
                    );
                  },
                  itemCount: widget.detailsModel.slider.length,
                  pagination: SwiperPagination(),
                  control: SwiperControl(),
                ),
              ),
              constUserLoginModel.id == 0
                  ? Container()
                  : Positioned.directional(
                      textDirection:
                          isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                      end: 30,
                      top: 20,
                      child: InkWell(
                        onTap: () {
                          print('star tapped');
                          widget.detailsModel.isLike
                              ? deleteFromFavourite(widget.detailsModel.id)
                              : addToFavourite(widget.detailsModel.id);
                          print('isLike:' +
                              widget.detailsModel.isLike.toString());
                          //TODO ask Sayed why it return false
                        },
                        child: CircleAvatar(
                          radius: 25,
                          backgroundColor: primaryColor,
                          child: CircleAvatar(
                            backgroundImage: AssetImage(
                                widget.detailsModel.isLike
                                    ? 'assets/star.png'
                                    : 'assets/star-in.png'),
                            radius: 16,
                            backgroundColor: primaryColor,
                          ),
                        ),
                      ),
                    ),
              Positioned.directional(
                textDirection:
                    isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                start: 30,
                bottom: 0,
                child: CircleAvatar(
                  radius: 40,
                  backgroundImage: NetworkImage(
                    widget.detailsModel.image,
                  ),
                  backgroundColor: Colors.transparent,
                ),
              ),
              Positioned.directional(
                  textDirection:
                      isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                  start: 140,
                  bottom: 0,
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(widget.detailsModel.name),
                        Text(widget.detailsModel.subCategory.name), 
                      ],
                    ),
                  ))
            ],
          ),
        ),
        SizedBox(height: 10)
      ],
    );
  }

  addToFavourite(int storeId) async {
    Map<String, dynamic> jsonMap = {"main_store_id": storeId};

    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'favorites',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      if (mounted) {
        setState(() {
          widget.detailsModel.isLike = true;
        });
      }
    } else {}
  }

  deleteFromFavourite(int storeId) async {
    ApiResponseModel responseModel = await NetworkUtil.delete(
      'favorites/$storeId',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      if (mounted) {
        setState(() {
          widget.detailsModel.isLike = false;
        });
      }
    } else {}
  }
}
