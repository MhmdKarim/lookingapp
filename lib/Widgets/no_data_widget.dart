import 'package:flutter/material.dart';
import 'package:lookin/PreDefined/app_constants.dart';
import 'package:lookin/Utils/size_config.dart';
import 'package:lookin/Widgets/text_widgets.dart';

class NoDataAvailable extends StatelessWidget {
  final String image;
  final String text;
  final bool visible;
  final Function onPressed;
  const NoDataAvailable({
    this.image,
    this.text,
    this.onPressed,
    this.visible,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Container(
        height: (vBlock * 100) - 100,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              image,
              height: hBlock * 50,
              width: hBlock * 50,
              fit: BoxFit.contain,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: MyText.mText(context: context, text: text, color: accentColor),
            ),
            Visibility(
              visible: visible,
              child: IconButton(
                  icon: Icon(Icons.refresh),
                  color: primaryColor,
                  iconSize: hBlock * 20,
                  onPressed: onPressed),
            )
          ],
        ),
      ),
    );
  }
}
