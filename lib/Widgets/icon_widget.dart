import 'package:flutter/material.dart';
import 'package:lookin/PreDefined/app_constants.dart';

class CustomIcon extends StatelessWidget {
  final String image;
  final double radius;
  final double imageSize;

  const CustomIcon({this.image, this.radius, this.imageSize});
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: primaryColorLight,
      radius: radius,
      child: Image.asset('assets/$image.png',height: imageSize,),

    );
  }
}