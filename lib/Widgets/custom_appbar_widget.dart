import 'package:flutter/material.dart';
import 'package:lookin/PreDefined/app_constants.dart';
import 'package:lookin/Views/AuthBottomSheets/login_sheet.dart';
import 'package:lookin/Views/search_page.dart';
import 'package:lookin/Widgets/show_sheet.dart';
import 'package:lookin/Widgets/user_photo.dart';

import 'text_widgets.dart';

class CustomAppBar {
  static Widget appBar(BuildContext context, String title, Widget bottom) {
    return AppBar(
      elevation: 0,
      backgroundColor: primaryColorLight,
      title:
          MyText.mTextBold(context: context, text: title, color: Colors.black),
      centerTitle: true,
      bottom: bottom,
    );
  }

  static Widget appBarNL(BuildContext context, String title) {
    return AppBar(
      elevation: 0,
      backgroundColor: primaryColorLight,
      title: MyText.mTextNLBold(
          context: context, text: title, color: Colors.black),
      centerTitle: true,
    );
  }

  static Widget buildAppBarLogin(
      BuildContext context, Function onlogin, bool visible) {
    return Container(
      color: primaryColorLight,
     // margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: hBlock * 5),
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            
            height: 80,
            width: 80,
            child: UserPhoto(imageUrl: constUserLoginModel.image, height: 80)),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  MyText.mTextBold(
                    context: context,
                    text: 'Welcome',
                  ),
                  Text(
                    constUserLoginModel.name != ''
                        ? ', ${constUserLoginModel.name}'
                        : '',
                    style:
                        TextStyle(fontSize: mFont, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(
                height: 3,
              ),
              constUserLoginModel.name != ''
                  ? MyText.mText(
                      context: context,
                      text: 'Welcome_Again',
                      color: primaryColor)
                  : InkWell(
                      onTap: () => ShowSheet.showSheet(
                          context, LoginSheet(onlogin, context)),
                      child: MyText.mText(
                          context: context,
                          text: 'Please_Login',
                          color: primaryColor),
                    ),
            ],
          ),
          Spacer(),
          Visibility(
            visible: visible,
            child: IconButton(
                icon: Icon(Icons.search),
                color: primaryColor,
                iconSize: 35,
                onPressed: () {
                  Navigator.pushNamed(context, SearchPage.tag);
                }),
          ),
          //  NotificationIcon(),
        ],
      ),
    );
  }


}
