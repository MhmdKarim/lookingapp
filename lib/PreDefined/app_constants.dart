import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:lookin/Models/all_filtration_model.dart';
import 'package:lookin/Models/login_model.dart';
import '../Utils/size_config.dart';

int currentbarIndex = 0;
int appLanguageId = 1;
String appLanguageCode = 'en';
bool isRightToLeft = false;
UserModel constUserLoginModel;
bool isLoggedIn;
List<Area> locationChips = [];
List<Area> categoryChips = [];
double vBlock = SizeConfig.vBlock;
double hBlock = SizeConfig.hBlock;
double xsFont = SizeConfig.xsFont;

double sFont = SizeConfig.sFont;
double mFont = SizeConfig.mFont;
double lFont = SizeConfig.lFontSize;
double xlFont = SizeConfig.xlFont;
double screenWidth = SizeConfig.screenWidth;
double screenHeight = SizeConfig.screenHeight;
String mapApiKey = 'AIzaSyAr0Bxz9pPgJaZCjGq_PJeqYNfZglPqFRY';
const Color titleColor = const Color(0XFFa7bdca);
const Color textColor = const Color(0XFF4d555b);
const MaterialColor primaryColor = MaterialColor(0XFF00C28C, <int, Color>{});
const MaterialColor primaryColorLight =
    MaterialColor(0XFFF2FCF9, <int, Color>{});
const MaterialColor accentColor = MaterialColor(0XFFCCCCCC, <int, Color>{});
const MaterialColor canvusColor = MaterialColor(0XFFFFFFFF, <int, Color>{});
LocationData locationData;
EdgeInsets padding = EdgeInsets.symmetric(horizontal: hBlock * 5);
double constLong ; 
double constLat ; 

// Booking Status :
// 1 - New Booking
// 2 - Restaurant Accepted the Booking
// 3 - Client Attended the booking
// 4 - Client didn't attend the booking
// 5 - Client canceled the booking
// 6 - restaurant refused the booking

