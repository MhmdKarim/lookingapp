// // To parse this JSON data, do
// //
// //     final filterationCriteriaModel = filterationCriteriaModelFromJson(jsonString);

// import 'dart:convert';

// FilterationCriteriaModel filterationCriteriaModelFromJson(String str) => FilterationCriteriaModel.fromJson(json.decode(str));

// String filterationCriteriaModelToJson(FilterationCriteriaModel data) => json.encode(data.toJson());

// class FilterationCriteriaModel {
//     FilterationCriteriaModel({
//         this.categoryId,
//         this.subId,
//         this.countryId,
//         this.cityId,
//     });

//     int categoryId;
//     int subId;
//     int countryId;
//     int cityId;

//     factory FilterationCriteriaModel.fromJson(Map<String, dynamic> json) => FilterationCriteriaModel(
//         categoryId: json["category_id"],
//         subId: json["sub_id"],
//         countryId: json["country_id"],
//         cityId: json["city_id"],
//     );

//     Map<String, dynamic> toJson() => {
//         "category_id": categoryId,
//         "sub_id": subId,
//         "country_id": countryId,
//         "city_id": cityId,
//     };
// }
