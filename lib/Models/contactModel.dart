import 'dart:convert';

Contacts contactsFromJson(String str) => Contacts.fromJson(json.decode(str));

String contactsToJson(Contacts data) => json.encode(data.toJson());

class Contacts {
  int id;
  String address;
  String addressEn;
  String lat;
  String lng;
  String phone;
  String email;
  String facebook;
  String twitter;
  String linkedin;
  String youtube;
  String instagram;
  dynamic createdAt;
  dynamic updatedAt;

  Contacts({
    this.id,
    this.address,
    this.addressEn,
    this.lat,
    this.lng,
    this.phone,
    this.email,
    this.facebook,
    this.twitter,
    this.linkedin,
    this.youtube,
    this.instagram,
    this.createdAt,
    this.updatedAt,
  });

  factory Contacts.fromJson(Map<String, dynamic> json) => Contacts(
    id: json["id"],
    address: json["address"],
    addressEn: json["address_en"],
    lat: json["lat"],
    lng: json["lng"],
    phone: json["phone"],
    email: json["email"],
    facebook: json["facebook"],
    twitter: json["twitter"],
    linkedin: json["linkedin"],
    youtube: json["youtube"],
    instagram: json["instagram"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "address": address,
    "address_en": addressEn,
    "lat": lat,
    "lng": lng,
    "phone": phone,
    "email": email,
    "facebook": facebook,
    "twitter": twitter,
    "linkedin": linkedin,
    "youtube": youtube,
    "instagram": instagram,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}