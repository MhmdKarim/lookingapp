
import 'dart:convert';

AllFiltrationModel allFiltrationModelFromJson(String str) => AllFiltrationModel.fromJson(json.decode(str));

String allFiltrationModelToJson(AllFiltrationModel data) => json.encode(data.toJson());

class AllFiltrationModel {
    AllFiltrationModel({
        this.categories,
        this.subCategories,
        this.countries,
        this.cities,
        this.areas,
    });

    List<Category> categories;
    List<SubCategory> subCategories;
    List<Category> countries;
    List<Area> cities;
    List<Area> areas;

    factory AllFiltrationModel.fromJson(Map<String, dynamic> json) => AllFiltrationModel(
        categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
        subCategories: List<SubCategory>.from(json["sub_categories"].map((x) => SubCategory.fromJson(x))),
        countries: List<Category>.from(json["countries"].map((x) => Category.fromJson(x))),
        cities: List<Area>.from(json["cities"].map((x) => Area.fromJson(x))),
        areas: List<Area>.from(json["areas"].map((x) => Area.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
        "sub_categories": List<dynamic>.from(subCategories.map((x) => x.toJson())),
        "countries": List<dynamic>.from(countries.map((x) => x.toJson())),
        "cities": List<dynamic>.from(cities.map((x) => x.toJson())),
        "areas": List<dynamic>.from(areas.map((x) => x.toJson())),
    };
}

class Area {
    Area({
        this.id,
        this.name,
        this.countryId,
        this.isSelected

    });

    int id;
    String name;
    int countryId;
    bool isSelected;

     Area.fromJson(Map<String, dynamic> json) {
        id = json["id"];
        name= json["name"];
        countryId= json["country_id"];
        isSelected =false;
     }

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "country_id": countryId,
    };
}

class Category {
    Category({
        this.id,
        this.name,
        this.isSelected
    });

    int id;
    String name;
    bool isSelected;

    factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        isSelected: false
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}

class SubCategory {
    SubCategory({
        this.id,
        this.name,
        this.categoryId,
        this.isSelected
    });

    int id;
    String name;
    int categoryId;
    bool isSelected;

    factory SubCategory.fromJson(Map<String, dynamic> json) => SubCategory(
        id: json["id"],
        name: json["name"],
        categoryId: json["category_id"],
        isSelected: false
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "category_id": categoryId,
    };
}
