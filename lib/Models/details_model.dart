// import 'login_model.dart';

// class DetailsModel {
//   int id;
//   String name;
//   String image;
//   String desc;
//   String terms;
//   List<String> slider;
//   List<Branchs> branchs;
//   bool isLike;
//   UserRate rate;
//   int rateCount;
  
//   DetailsModel(
//       {this.id,
//       this.name,
//       this.image,
//       this.desc,
//       this.terms,
//       this.slider,
//       this.branchs,
//       this.isLike,
//       this.rate,
//       this.rateCount});

//   DetailsModel.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     image = json['image'];
//     desc = json['desc'];
//     terms = json['terms'];
//     slider = json['slider'].cast<String>();
//     if (json['branchs'] != null) {
//       branchs = new List<Branchs>();
//       json['branchs'].forEach((v) {
//         branchs.add(new Branchs.fromJson(v));
//       });
//     }
//     isLike = json['is_like'];
//     rate = json['rate'] != null ? new UserRate.fromJson(json['rate']) : null;
//     rateCount = json['rate_count'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['image'] = this.image;
//     data['desc'] = this.desc;
//     data['terms'] = this.terms;
//     data['slider'] = this.slider;
//     if (this.branchs != null) {
//       data['branchs'] = this.branchs.map((v) => v.toJson()).toList();
//     }
//     data['is_like'] = this.isLike;
//     if (this.rate != null) {
//       data['rate'] = this.rate.toJson();
//     }
//     data['rate_count'] = this.rateCount;
//     return data;
//   }
// }

// class Branchs {
//   int id;
//   String address;
//   double longitude;
//   double latitude;
//   bool isSelected; 

//   Branchs(
//       {this.id, this.address, this.longitude, this.latitude, this.isSelected});

//   Branchs.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     address = json['address'];
//     longitude = json['longitude'];
//     latitude = json['latitude'];
//     isSelected = false;
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['address'] = this.address;
//     data['longitude'] = this.longitude;
//     data['latitude'] = this.latitude;
//     return data;
//   }
// }

// class UserRate {
//   int id;
//   String comment;
//   int rate;
//   UserModel user;

//   UserRate({this.id, this.comment, this.rate, this.user});

//   UserRate.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     comment = json['comment'];
//     rate = json['rate'];
//     user = json['user'] != null ? new UserModel.fromJson(json['user']) : null;
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['comment'] = this.comment;
//     data['rate'] = this.rate;
//     if (this.user != null) {
//       data['user'] = this.user.toJson();
//     }
//     return data;
//   }
// }
import 'package:lookin/Models/login_model.dart';

class DetailsModel {
  int id;
  String name;
  String image;
  String desc;
  String terms;
  List<String> slider;
  List<Branchs> branchs;
  bool isLike;
  UserRate rate;
  int rateCount;
  SubCat subCategory;

  DetailsModel(
      {this.id,
      this.name,
      this.image,
      this.desc,
      this.terms,
      this.slider,
      this.branchs,
      this.isLike,
      this.rate,
      this.rateCount,
      this.subCategory});

  DetailsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    desc = json['desc'];
    terms = json['terms'];
    slider = json['slider'].cast<String>();
    if (json['branchs'] != null) {
      branchs = new List<Branchs>();
      json['branchs'].forEach((v) {
        branchs.add(new Branchs.fromJson(v));
      });
    }
    isLike = json['is_like'];
    rate = json['rate'] != null ? new UserRate.fromJson(json['rate']) : null;
    rateCount = json['rate_count'];
    subCategory = json['sub_category'] != null
        ? new SubCat.fromJson(json['sub_category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    data['desc'] = this.desc;
    data['terms'] = this.terms;
    data['slider'] = this.slider;
    if (this.branchs != null) {
      data['branchs'] = this.branchs.map((v) => v.toJson()).toList();
    }
    data['is_like'] = this.isLike;
    data['rate'] = this.rate;
    data['rate_count'] = this.rateCount;
    if (this.subCategory != null) {
      data['sub_category'] = this.subCategory.toJson();
    }
    return data;
  }
}

class Branchs {
  int id;
  String address;
  double longitude;
  double latitude;
  int available;
bool isSelected; 
  Branchs(
      {this.id, this.address, this.longitude, this.latitude, this.available,this.isSelected});

  Branchs.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    address = json['address'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    available = json['available'];
    isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['address'] = this.address;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['available'] = this.available;
    return data;
  }
}

class SubCat {
  int id;
  String name;

  SubCat({this.id, this.name});

  SubCat.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
class UserRate {
  int id;
  String comment;
  int rate;
  UserModel user;

  UserRate({this.id, this.comment, this.rate, this.user});

  UserRate.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    comment = json['comment'];
    rate = json['rate'];
    user = json['user'] != null ? new UserModel.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['comment'] = this.comment;
    data['rate'] = this.rate;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}