import 'bookings_model.dart';

class AllInBoxModel {
  int currentPage;
  List<AllInBoxDataModel> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;

  AllInBoxModel(
      {this.currentPage,
      this.data,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  AllInBoxModel.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<AllInBoxDataModel>();
      json['data'].forEach((v) {
        data.add(new AllInBoxDataModel.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class AllInBoxDataModel {
  int id;
  Restaurant restaurant;
  LastInbox lastInbox;

  AllInBoxDataModel({this.id, this.restaurant, this.lastInbox});

  AllInBoxDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    restaurant = json['restaurant'] != null
        ? new Restaurant.fromJson(json['restaurant'])
        : null;
    lastInbox = json['last_inbox'] != null
        ? new LastInbox.fromJson(json['last_inbox'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.restaurant != null) {
      data['restaurant'] = this.restaurant.toJson();
    }
    if (this.lastInbox != null) {
      data['last_inbox'] = this.lastInbox.toJson();
    }
    return data;
  }
}


class LastInbox {
  int id;
  String msg;
  int senderType;
  int date;

  LastInbox({this.id, this.msg, this.senderType, this.date});

  LastInbox.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    msg = json['msg'];
    senderType = json['sender_type'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['msg'] = this.msg;
    data['sender_type'] = this.senderType;
    data['date'] = this.date;
    return data;
  }
}

