import 'dart:convert';
import 'all_filtration_model.dart';

HomeFilterModel homeFilterModelFromJson(String str) => HomeFilterModel.fromJson(json.decode(str));

String homeFilterModelToJson(HomeFilterModel data) => json.encode(data.toJson());

class HomeFilterModel {
    HomeFilterModel({
        this.categories,
        this.subCategories,
        this.countries,
        this.cities,
        this.areas,
    });

    List<Area> categories;
    List<Area> subCategories;
    List<Area> countries;
    List<Area> cities;
    List<Area> areas;

    factory HomeFilterModel.fromJson(Map<String, dynamic> json) => HomeFilterModel(
        categories: List<Area>.from(json["categories"].map((x) => Area.fromJson(x))),
        subCategories: List<Area>.from(json["sub_categories"].map((x) => Area.fromJson(x))),
        countries: List<Area>.from(json["countries"].map((x) => Area.fromJson(x))),
        cities: List<Area>.from(json["cities"].map((x) => Area.fromJson(x))),
        areas: List<Area>.from(json["areas"].map((x) => Area.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
        "sub_categories": List<dynamic>.from(subCategories.map((x) => x.toJson())),
        "countries": List<dynamic>.from(countries.map((x) => x.toJson())),
        "cities": List<dynamic>.from(cities.map((x) => x.toJson())),
        "areas": List<dynamic>.from(areas.map((x) => x.toJson())),
    };
}

