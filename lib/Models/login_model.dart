

class UserModel {
  int id;
  String name;
  String phone;
  String image;
  String rememberToken;

  UserModel({this.id, this.name, this.phone, this.image, this.rememberToken});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    image = json['image'];
    rememberToken = json['remember_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['image'] = this.image;
    data['remember_token'] = this.rememberToken;
    return data;
  }
}
