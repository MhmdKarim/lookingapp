import 'package:flutter/material.dart';
import 'package:lookin/PreDefined/app_constants.dart';
import 'package:lookin/PreDefined/localization.dart';
import 'package:lookin/Utils/size_config.dart';
import 'package:lookin/Widgets/text_widgets.dart';

class MorePage extends StatefulWidget {
  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  AppLocalizations appLocalizations;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
            title: MyText.mTextBold(
              context: context,
          text: 'Welcome',
        )),
        body: Container(),
      ),
    );
  }
}
