import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/thanks_sheet.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import '../../Widgets/button.dart';
import '../../Widgets/text_widgets.dart';

class ContactUsPage extends StatefulWidget {
  static const tag = '/ContactUsPage';

  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {
  // GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  AppLocalizations appLocalizations;
  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _messageController = TextEditingController();
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  final _formKey = GlobalKey<FormState>();

@override
  void dispose() {
_nameController.dispose();
_phoneController.dispose();
_messageController.dispose();
    super.dispose();
  }

  void _showThanksSheet(BuildContext ctx) {
    showModalBottomSheet<dynamic>(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      context: ctx,
      builder: (context) {
        return Wrap(
          children: <Widget>[
            ThanksSheet(
              title: 'Ur_message_sent_successfully',
              message: 'Contact_you_soon',
              buttonTitle: 'Thank_You',
              function: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, "contact_us",null),
        body: ListView(
          padding: padding,
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: vBlock * 30,
                      padding: const EdgeInsets.all(20.0),
                      child: Center(
                        child: Image.asset(
                          'assets/support-page.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    MyText.mText(
                      context: context,
                      text: 'Name',
                      color: accentColor,
                    ),
                    TextFormField(
                      controller: _nameController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return  appLocalizations.getMessageByLangAndKey(
                                      appLanguageCode, 'Enter') +
                                  ' ' +
                                  appLocalizations.getMessageByLangAndKey(
                                      appLanguageCode, 'Name');
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    MyText.mText(
                      context: context,
                      text: 'Phone_Number',
                      color: accentColor,
                    ),
                    TextFormField(
                      controller: _phoneController,
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return appLocalizations.getMessageByLangAndKey(
                                      appLanguageCode, 'Enter') +
                                  ' ' +
                                  appLocalizations.getMessageByLangAndKey(
                                      appLanguageCode, 'Phone_Number');
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    MyText.mText(
                      context: context,
                      text: 'Write_your_Request',
                      color: accentColor,
                    ),
                    TextFormField(
                      controller: _messageController,
                      maxLines: 5,
                      validator: (value) {
                        if (value.isEmpty) {
                          return appLocalizations.getMessageByLangAndKey(
                                      appLanguageCode, 'Write_your_Request') 
                                  ;
                        }
                        return null;
                      },
                    )
                  ],
                ),
              ),
            ),
            RoundedLoadingButton(
              color: primaryColor,
              width: hBlock * 85,
              controller: _btnController,
              onPressed: () {
                if (!_formKey.currentState.validate()) {
                  _btnController.reset();
                  return;
                }
                postContactUs();
                _btnController.reset();
              },
              child: MyText.mText(
                  context: context, text: "contact_us", color: Colors.white),
            ),
            SizedBox(height: 20)
          ],
        ),
      ),
    );
  }

  postContactUs() async {
    Map<String, dynamic> jsonMap = {
      "name": _nameController.text,
      "phone": _phoneController.text,
      "msg": _messageController.text
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'contactUs',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      print(responseModel.message);
      _btnController.success();
      setState(() {
        Navigator.of(context).pop();
        _showThanksSheet(context);
      });
    } else {
      _btnController.reset();
    }
  }
}
// {
//     "name": "Sayed Admin",
//     "phone": "123456",
//     "msg": "try to be special"
// }
