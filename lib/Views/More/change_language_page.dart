import 'package:flutter/material.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/text_widgets.dart';
import '../../Generics/app_prefs.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';

class ChangeLanguagePage extends StatefulWidget {
  static const tag = '/ChangeLanguagePage';

  @override
  _ChangeLanguagePageState createState() => _ChangeLanguagePageState();
}

class _ChangeLanguagePageState extends State<ChangeLanguagePage> {
  AppLocalizations appLocalizations;
  Color _enbuttonColor = primaryColor;
  Color _arbuttonColor = accentColor;
  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, "change_language",null),
        body: ListView(
          children: <Widget>[
            Container(
              height: 100,
              margin: EdgeInsets.all(20),
              child: Center(
                child: Image.asset(
                  'assets/logo1.png',
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Column(
              children: <Widget>[
                MyText.mTextNLBold(
                    context: context, text: 'Select App Language'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: MyText.mTextNLBold(
                      context: context, text: 'اختر لغة التطبيق'),
                ),
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Center(
                    child: Image.asset(
                      'assets/language.png',
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      color: _enbuttonColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          5.0,
                        ),
                      ),
                      child: MyText.sTextNL(context: context, text: 'English'),
                      onPressed: () {
                        setState(() {
                          appLanguageId = 1;
                          appLanguageCode = 'en';
                          isRightToLeft = false;
                          savePrefsInt('selected_language', 1);
                          _arbuttonColor = accentColor;
                          _enbuttonColor = primaryColor;
                        });
                        Navigator.of(context).pop();
                      },
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    RaisedButton(
                      color: _arbuttonColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          5.0,
                        ),
                      ),
                      child: MyText.sTextNL(context: context, text: 'العربية'),
                      onPressed: () {
                        setState(() {
                          appLanguageId = 2;
                          appLanguageCode = 'ar';
                          isRightToLeft = true;
                          savePrefsInt('selected_language', 2);
                          _arbuttonColor = primaryColor;
                          _enbuttonColor = accentColor;
                        });
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 100,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
