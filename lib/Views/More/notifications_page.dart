// import 'package:flutter/material.dart';
// import 'package:lookin/Widgets/custom_appbar_widget.dart';
// import '../../PreDefined/app_constants.dart';
// import '../../PreDefined/localization.dart';
// import '../../Utils/size_config.dart';

// class NotificationsPage extends StatefulWidget {
//     static const tag = '/NotificationsPage';

//   @override
//   _NotificationsPageState createState() => _NotificationsPageState();
// }

// class _NotificationsPageState extends State<NotificationsPage> {
//   AppLocalizations appLocalizations;

//   @override
//   Widget build(BuildContext context) {
//     SizeConfig.init(context);
//     appLocalizations = AppLocalizations.of(context);
//     return Directionality(
//       textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
//       child: Scaffold(
//         appBar: CustomAppBar.appBar(context, "notifications",null),
//         body: Container(),
//       ),
//     );
//   }
// }
