import 'package:flutter/material.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/PreDefined/localization.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import '../../PreDefined/app_constants.dart';
import '../../Utils/size_config.dart';

class AboutUsPage extends StatefulWidget {
  static const tag = '/AboutUsPage';
  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {
  AppLocalizations appLocalizations;
  bool _isLoading = true;
  String _aboutUs;
  @override
  void initState() {
    _getAboutUs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, "About_us",null),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
              padding: padding,
                child: Column(
                  children: <Widget>[
                    Container(
                        height: hBlock * 50,
                        width: hBlock * 95,
                        //padding: EdgeInsets.all(8),
                        child: Image.asset(
                          'assets/aboutus.png',
                          fit: BoxFit.contain,
                        )),
                    Container(
                      child: Center(child: Text(_aboutUs)),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  _getAboutUs() async {
    // if (constUserLoginModel == null) {
    //   return;
    // }
    ApiResponseModel responseModel = await NetworkUtil.getWithoutToken(
      'about',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      _aboutUs = responseModel.data["about"];
      print(responseModel.statuscode);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }
}
