import 'package:flutter/material.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import '../../PreDefined/localization.dart';
import '../../PreDefined/app_constants.dart';
import '../../Utils/size_config.dart';

class TermsConditionsPage extends StatefulWidget {
  static const tag = '/TermsConditionsPage';

  @override
  _TermsConditionsPageState createState() => _TermsConditionsPageState();
}

class _TermsConditionsPageState extends State<TermsConditionsPage> {
  AppLocalizations appLocalizations;
  bool _isLoading = true;
  String _termsConditions;
  @override
  void initState() {
    _getTermsConditions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, "terms_conditions",null),
        body: Container(
          padding: padding,
          child: _isLoading
              ? Center(child: CircularProgressIndicator())
              : Center(child: Text(_termsConditions)),
        ),
      ),
    );
  }

  _getTermsConditions() async {

    ApiResponseModel responseModel = await NetworkUtil.getWithoutToken(
      'terms&conditions',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      _termsConditions = responseModel.data["terms"];
      print(responseModel.statuscode);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }
}
