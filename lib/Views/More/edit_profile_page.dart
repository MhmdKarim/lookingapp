import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Generics/app_prefs.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Models/login_model.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/user_photo.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import '../../Widgets/button.dart';
import '../../Widgets/text_widgets.dart';

class EditProfilePage extends StatefulWidget {
  static const tag = '/EditProfilePage';

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  AppLocalizations appLocalizations;
  final _nameController = TextEditingController(text: constUserLoginModel.name);
  final _phoneController = TextEditingController();
 // final _passwordController = TextEditingController();
  String base64Image;
  File userProfileImage;
  final _formKey = GlobalKey<FormState>();

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  @override
  void initState() {
    networkImageToBase64(constUserLoginModel.image).then((onValue){
      base64Image = onValue;
    });
    super.initState();
  }
  @override
  void dispose() {
_nameController.dispose();
_phoneController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: true,
        appBar: CustomAppBar.appBar(context, "Edit_Profile",null),
        body: SingleChildScrollView(
          padding: padding,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  imageWidget(),
                  Row(
                    children: <Widget>[
                      MyText.mText(
                        context: context,
                        text: 'Name',
                        color: accentColor,
                      ),
                    ],
                  ),
                  TextFormField(
                    controller: _nameController,
                    cursorColor: primaryColor,
                    // decoration: InputDecoration(
                    //     hintText: constUserLoginModel.name,
                    //   ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter right ....';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      MyText.mText(
                        context: context,
                        text: 'Phone_Number',
                        color: accentColor,
                      ),
                    ],
                  ),
                  TextFormField(
                    enabled: false,
                    controller: _phoneController,
                    cursorColor: primaryColor,
                    decoration: InputDecoration(
                      hintText: constUserLoginModel.phone,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter right ....';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // Row(
                  //   children: <Widget>[
                  //     MyText.mText(
                  //       context: context,
                  //       text: 'Password',
                  //       color: accentColor,
                  //     ),
                  //   ],
                  // ),
                  // TextFormField(
                  //   controller: _passwordController,
                  //   cursorColor: primaryColor,
                  //   validator: (value) {
                  //     if (value.isEmpty) {
                  //       return 'Please enter right ....';
                  //     }
                  //     return null;
                  //   },
                  // ),

                  SizedBox(
                    height: 20,
                  ),
                  RoundedLoadingButton(
                    // curve: Curves.ease,
                    //width: screenWidth * .6,
                    color: primaryColor,
                    width: hBlock * 85,
                    controller: _btnController,
                    onPressed: () {
                      // if (!_formKey.currentState.validate()) {
                      //   _btnController.reset();
                      //   return;
                      // }
                      postEditProfile();
                    },
                    child: MyText.mText(
                        context: context,
                        text: "Edit_Profile",
                        color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget imageWidget() {
    return Container(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Center(
        child: Stack(
          overflow: Overflow.visible,
          fit: StackFit.passthrough,
          //fit: StackFit.loose,
          children: <Widget>[
            ClipRRect(
                child: userProfileImage == null
                    ?
                    UserPhoto(
                       
                        imageUrl: constUserLoginModel.image,
                         height: hBlock * 30,
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            shape: BoxShape.circle),
                        child: CircleAvatar(
                          backgroundImage: FileImage(userProfileImage),
                          backgroundColor: Colors.transparent,
                          maxRadius: 57,
                        ),
                        height: hBlock * 30,
                        width: hBlock * 30,
                      )),
            Positioned(
              top: 0,
              bottom: 0,
              right: -hBlock * 6,
              child: Container(
                height: hBlock * 12,
                width: hBlock * 12,
                decoration: BoxDecoration(
                    border: Border.all(
                        width: 2, color: Theme.of(context).accentColor),
                    color: canvusColor,
                    shape: BoxShape.circle),
                child: IconButton(
                  onPressed: () {
                    _openChooseImagePopup();
                  },
                  icon: Icon(
                    Icons.edit,
                    size: hBlock * 7,
                    color: primaryColor,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Widget _showImage(Future<File> _imageFile) {
  //   return FutureBuilder<File>(
  //     future: _imageFile,
  //     builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
  //       if (snapshot.connectionState == ConnectionState.done &&
  //           snapshot.data != null) {
  //         return ClipOval(
  //           child: Image.file(snapshot.data, fit: BoxFit.cover),
  //         );
  //       } else if (snapshot.error != null) {
  //         return const Text(
  //           'Error Picking Image',
  //           textAlign: TextAlign.center,
  //         );
  //       }
  //       if (snapshot.data == null) {
  //         return Center(
  //           child: const Text(
  //             'No Image Selected',
  //             textAlign: TextAlign.center,
  //           ),
  //         );
  //       } else {
  //         return const Center(
  //           child: CircularProgressIndicator(
  //             backgroundColor: primaryColor,
  //           ),
  //         );
  //       }
  //     },
  //   );
  // }

  _pickImage(ImageSource _imageSource) async {
     final _picker = ImagePicker();
    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
   final File file = File(pickedFile.path);
    List<int> imageBytes = file.readAsBytesSync();
    base64Image = base64Encode(imageBytes);
    if (file != null) {
      setState(() {
        userProfileImage = file;
      });
    }
  }

  Future<String> networkImageToBase64(String imageUrl) async {
    http.Response response = await http.get(imageUrl);
    final bytes = response?.bodyBytes;
    return (bytes != null ? base64Encode(bytes) : null);
  }

  _openChooseImagePopup() {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          title: Text(
            'choose photo',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: hBlock * 6,
                fontWeight: FontWeight.bold,
                color: primaryColor),
          ),
          children: <Widget>[
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                _pickImage(ImageSource.gallery);
              },
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.photo,
                    color: Theme.of(context).primaryColor,
                    size: hBlock * 8,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text('From Gallery',
                      textAlign: TextAlign.start,
                      style: TextStyle(fontSize: hBlock * 4)),
                ],
              ),
            ),
            Divider(
              color: Theme.of(context).accentColor,
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                _pickImage(ImageSource.camera);
              },
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.camera_alt,
                    color: Theme.of(context).primaryColor,
                    size: hBlock * 8,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text('By camera',
                      textAlign: TextAlign.start,
                      style: TextStyle(fontSize: hBlock * 4)),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  postEditProfile() async {
    Map<String, dynamic> jsonMap = {
      'name': _nameController.text,
      'image': base64Image
    };
    String jsonString = json.encode(jsonMap);
    print(jsonString);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'profile',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      UserModel model = UserModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      String loginModel = json.encode(model);
      print(loginModel);
      savePrefs('loginModel', loginModel);
      savePrefsbool('isLoggedIn', true);
      constUserLoginModel = model;
      isLoggedIn = true;
      _btnController.success();
      setState(() {
        Navigator.of(context).pop();
      });
    } else {
      _btnController.reset();
    }
  }
}
