import 'package:flutter/material.dart';
import 'package:lookin/Generics/app_prefs.dart';
import 'package:lookin/Views/StartPages/splash_screen.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/icon_widget.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import 'About_us_page.dart';
import 'change_language_page.dart';
import 'contact_us_page.dart';
import 'edit_profile_page.dart';
import 'messages_page.dart';
import 'terms_conditions_page.dart';
import '../../Widgets/alert_widget.dart';
import '../../Widgets/text_widgets.dart';
import 'privacy_policy_page.dart';

class MorePage extends StatefulWidget {
  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  AppLocalizations appLocalizations;

  _onLogin() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return SafeArea(
          child: Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: Scaffold(
          body: Stack(
            children: <Widget>[
              Positioned.directional(
                  textDirection:
                      isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                  top: 0.0,
                  start: 0.0,
                  end: 0.0,
                  child: CustomAppBar.buildAppBarLogin(context, _onLogin, true)),
              Container(
                padding: EdgeInsets.only(
                    top: 80, left: hBlock * 5, right: hBlock * 5),
                child: constUserLoginModel.id == 0
                    ? ListView(children: <Widget>[
                        _buildListTile(
                            context: context,
                            title: 'About_us',
                            image: 'info',
                            route: AboutUsPage.tag),
                        _buildListTile(
                            context: context,
                            title: 'change_language',
                            image: 'change-language',
                            route: ChangeLanguagePage.tag),
                        _buildListTile(
                            context: context,
                            title: 'terms_conditions',
                            image: 'terms',
                            route: TermsConditionsPage.tag),
                        _buildListTile(
                            context: context,
                            title: 'privacy_policy',
                            image: 'privacy',
                            route: PrivacyPolicyPage.tag),
                        _buildListTile(
                            context: context,
                            title: 'contact_us',
                            image: 'support',
                            route: ContactUsPage.tag),
                      ])
                    : ListView(
                        children: <Widget>[
                          _buildListTile(
                              context: context,
                              title: 'Update_Profile',
                              image: 'update-profile',
                              route: EditProfilePage.tag),
                          _buildListTile(
                              context: context,
                              title: 'About_us',
                              image: 'info',
                              route: AboutUsPage.tag),
                          // _buildListTile(
                          //     context: context,
                          //     title: 'notifications',
                          //     image: 'notification',
                          //     route: NotificationsPage.tag),
                          _buildListTile(
                              context: context,
                              title: 'messages',
                              image: 'messages',
                              route: MessagesPage.tag),
                          _buildListTile(
                              context: context,
                              title: 'change_language',
                              image: 'change-language',
                              route: ChangeLanguagePage.tag),
                          _buildListTile(
                              context: context,
                              title: 'terms_conditions',
                              image: 'terms',
                              route: TermsConditionsPage.tag),
                          _buildListTile(
                              context: context,
                              title: 'privacy_policy',
                              image: 'privacy',
                              route: PrivacyPolicyPage.tag),
                          _buildListTile(
                              context: context,
                              title: 'contact_us',
                              image: 'support',
                              route: ContactUsPage.tag),
                          ListTile(
                            leading: CustomIcon(
                              radius: 25,
                              image: 'logout',
                              imageSize: 25,
                            ),
                            title: MyText.mText(
                                context: context,
                                text: 'exit',
                                color: Colors.red),
                            onTap: () {
                              // Navigator.pushNamed(context, route);
                              MyPopup.showPopUp(
                                context: context,
                                title: appLocalizations.getMessageByLangAndKey(appLanguageCode, 'confirmation'),
                                message: appLocalizations.getMessageByLangAndKey(appLanguageCode, 'are_you_sure_exit'),
                                function: () {
                                  savePrefs('loginModel', null).then((onValue) {
                                    //constUserLoginModel = null;
                                  });
                                  savePrefsbool('isLoggedIn', false);
                                  isLoggedIn = false;
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                        builder: (context) => SplashScreenPage(),
                                      ),
                                      (Route<dynamic> route) => false);
                                },
                              );
                            },
                          )
                        ],
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildListTile(
      {BuildContext context, String title, String image, String route}) {
    return ListTile(
      leading: CustomIcon(radius: 25, image: image, imageSize: 25),
      title: MyText.mText(
        context: context,
        text: title,
      ),
      trailing: Icon(Icons.arrow_forward_ios),
      onTap: () {
        Navigator.pushNamed(context, route);
      },
    );
  }
}
