import 'package:flutter/material.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import '../../PreDefined/localization.dart';
import '../../PreDefined/app_constants.dart';
import '../../Utils/size_config.dart';

class PrivacyPolicyPage extends StatefulWidget {
  static const tag = '/PrivacyPolicyPage';

  @override
  _PrivacyPolicyPageState createState() => _PrivacyPolicyPageState();
}

class _PrivacyPolicyPageState extends State<PrivacyPolicyPage> {
  AppLocalizations appLocalizations;
  bool _isLoading = true;
  String _privacyPolicy;

  @override
  void initState() {
    _getPrivacyPolicy();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, "privacy_policy",null),
        body: Container(
          padding: padding,
          child: _isLoading
              ? Center(child: CircularProgressIndicator())
              : Center(child: Text(_privacyPolicy)),
        ),
      ),
    );
  }

  _getPrivacyPolicy() async {
    // if (constUserLoginModel == null) {
    //   return;
    // }
    ApiResponseModel responseModel = await NetworkUtil.getWithoutToken(
      'privacy&policy',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      _privacyPolicy = responseModel.data["privacy"];
      print(responseModel.statuscode);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }
}
