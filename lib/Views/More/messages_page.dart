import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lookin/Generics/GenericFunctions.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/all_inbox_model.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Views/chat_page.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';

class MessagesPage extends StatefulWidget {
  static const tag = '/MessagesPage';

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  AppLocalizations appLocalizations;
  bool _isLoading = true;
  List<AllInBoxDataModel> _messagesList;
  int _pageAll = 1;
  int _perPageAll;
  int _lastPage = 1;

  Future<void> _refreshMessages() async {
    if (_pageAll > _lastPage) {
      _isLoading = false;
      return;
    } else {
      setState(() {
        print(' I am refreshing data');
        _getMessages();
      });
    }
  }

  @override
  void initState() {
    _getMessages();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, "messages", null),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : ListView(
              padding: padding,
                children: <Widget>[
                  LazyLoadScrollView(
                    scrollOffset: 0,
                    onEndOfPage: _refreshMessages,
                    child: RefreshIndicator(
                      onRefresh: _refreshMessages,
                      child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: _messagesList.length,
                        itemBuilder: (context, index) {
                          return _buildItems(index);
                        },
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  Widget _buildItems(int index) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChatPage(
              title: _messagesList[index].restaurant.name,
              id: _messagesList[index].id,
            ),
          ),
        );
      },
      child: Container(
        height: 100,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: 1,
                color: Colors.transparent,
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircleAvatar(
                      radius: 30,
                      backgroundImage: NetworkImage(
                        _messagesList[index].restaurant.image,
                      ),
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                  Container(
                    width: hBlock * 65,
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(_messagesList[index].restaurant.name),
                            Text(
                              GenericFunctions.intToDate(
                                  _messagesList[index].lastInbox.date),
                            ),
                          ],
                        ),
                        Text(
                          _messagesList[index].lastInbox.msg,
                          maxLines: 2,
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                height: 1,
                color: accentColor,
              )
            ],
          ),
        ),
      ),
    );
  }

  _getMessages() async {
    ApiResponseModel responseModel = await NetworkUtil.get(
      'inbox?page=$_pageAll',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      AllInBoxModel model = AllInBoxModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _lastPage = model.lastPage;
      _perPageAll = model.perPage;
      _pageAll++;
      _messagesList = model.data;
      print(_perPageAll);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = true;
    }
  }
}
