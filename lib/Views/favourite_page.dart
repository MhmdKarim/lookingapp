import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Models/home_model.dart';
import 'package:lookin/Views/Details/details_page.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/home_photo_widget.dart';
import 'package:lookin/Widgets/no_data_widget.dart';
import '../PreDefined/app_constants.dart';
import '../PreDefined/localization.dart';
import '../Utils/size_config.dart';

class FavouritePage extends StatefulWidget {
  static const tag = '/FavouritePage';
  @override
  _FavouritePageState createState() => _FavouritePageState();
}

class _FavouritePageState extends State<FavouritePage> {
  AppLocalizations appLocalizations;
  List<RestaurantsData> _restaurantsData = [];
  bool _isLoading = true;
  int _pageAll = 1;
  int _lastPage = 1;

  @override
  void initState() {
    getData();
    super.initState();
  }

  Future<void> _refreshAllData() async {
    if (_pageAll > _lastPage) {
      _isLoading = false;
      return;
    } else {
      setState(() {
        print(' I am refreshing data');
        getData();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, 'Favourites', null),
        body: constUserLoginModel.id == 0
            ? NoDataAvailable(
                image: 'assets/star-empty.png',
                text: 'Favourites_are_empty',
                visible: false,
              )
            : _isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : _restaurantsData.length == 0
                    ? NoDataAvailable(
                        image: 'assets/star-empty.png',
                        text: 'Favourites_are_empty',
                        visible: true,
                        onPressed: _refreshAllData,
                      )
                    : LazyLoadScrollView(
                        scrollOffset: 0,
                        onEndOfPage: _refreshAllData,
                        child: RefreshIndicator(
                          onRefresh: _refreshAllData,
                          child: ListView.builder(
                             padding: padding,
                            // controller: _allController,
                            scrollDirection: Axis.vertical,
                            physics: ScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: _restaurantsData.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DetailsPage(
                                            // id: _restaurantsData[index].id,
                                            // title: _restaurantsData[index].name,
                                            restaurantData:
                                                _restaurantsData[index])),
                                  );
                                },
                                child: HomePhotoWidget(_restaurantsData[index]),
                              );
                            },
                          ),
                        ),
                      ),
      ),
    );
  }

  getData() async {
    ApiResponseModel responseModel = await NetworkUtil.get(
      'favorites?page=$_pageAll',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      Restaurants model = Restaurants.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _lastPage = model.lastPage;
      _pageAll++;
      _restaurantsData.addAll(model.data);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }
}
