import 'dart:async';
import 'dart:convert';
import 'package:app_settings/app_settings.dart';

import 'package:flutter/material.dart';
import 'package:location/location.dart';
import '../../Generics/app_prefs.dart';
import '../../Models/login_model.dart';
import '../../Widgets/dashboard.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Views/StartPages/language_page.dart';

class SplashScreenPage extends StatefulWidget {
  static const tag = '/SplashScreenPage';
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  AppLocalizations appLocalizations;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  PermissionStatus _permissionGranted;
  final Location location = Location();
 // String _error;

  _readDataFromSharedPref() async {
    await Future.delayed(Duration(seconds: 2));
  
    
    var userLoggedModel = await getpreferences('loginModel');
    if (userLoggedModel == null) {
      currentbarIndex = 0;
      print('splash not logged');
      constUserLoginModel =
          UserModel(id: 0, name: '', phone: '', image: '', rememberToken: '');
    } else {
      currentbarIndex = 0;
      constUserLoginModel = UserModel.fromJson(json.decode(userLoggedModel));
      print(constUserLoginModel.rememberToken);
      print(constUserLoginModel.image);
      Navigator.pushReplacementNamed(
          _scaffoldKey.currentContext, Dashboard.tag);
      return;
    }

    var _selectedLanguage = await getpreferencesInt('selected_language');
    if (_selectedLanguage == null) {
      appLanguageId = 1;
      isRightToLeft = false;
      currentbarIndex = 0;
      Navigator.pushReplacementNamed(
          _scaffoldKey.currentContext, LanguagePage.tag);
      return;
    } else {
      appLanguageId = _selectedLanguage;
      currentbarIndex = 0;
      if (appLanguageId == 1) {
        appLanguageCode = 'en';
        isRightToLeft = false;
      } else {
        appLanguageCode = 'ar';
        isRightToLeft = true;
      }
      Navigator.pushReplacementNamed(
          _scaffoldKey.currentContext, Dashboard.tag);
    }
  }

  _location() async {
    bool _serviceEnabled;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();

      print('_serviceEnabled' + _serviceEnabled.toString());
      if (!_serviceEnabled) {
      //  AppSettings.openLocationSettings();
      
            _readDataFromSharedPref();
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        print('_permissionGranted.not');
        print('goto settings');
        AppSettings.openLocationSettings();
        return;
      }
    }

    locationData = await location.getLocation();
    setState(() {
      _readDataFromSharedPref();
    });

    print(locationData.longitude.toString() +
        '     ' +
        locationData.latitude.toString());
  }

  // Future<void> _getLocation() async {
  //   setState(() {
  //     _error = null;
  //   });
  //   try {
  //     final LocationData _locationResult = await location.getLocation();
  //     setState(() {
  //       locationData = _locationResult;
  //       constLong = _locationResult.longitude;
  //       constLat = _locationResult.latitude;

  //       //getAllData();
  //     });
  //     print('Long : ' + locationData.longitude.toString());
  //     print('Lat : ' + locationData.latitude.toString());
  //   } on PlatformException catch (err) {
  //     setState(() {
  //       _error = err.code;
  //     });
  //   }
  // }

  // Future<void> _checkPermissions() async {
  //   final PermissionStatus permissionGrantedResult =
  //       await location.hasPermission();
  //   setState(() {
  //     _permissionGranted = permissionGrantedResult;
  //   });
  // }

  // Future<void> _requestPermission() async {
  //   if (_permissionGranted != PermissionStatus.granted) {
  //     final PermissionStatus permissionRequestedResult =
  //         await location.requestPermission();
  //     setState(() {
  //       _permissionGranted = permissionRequestedResult;
  //       if (_permissionGranted == PermissionStatus.granted) {
  //         print('object 0');
  //         // _readDataFromSharedPref();
  //       } else {
  //         print('object 1');
  //       }
  //       // _refreshAllData();
  //     });
  //     if (permissionRequestedResult != PermissionStatus.granted) {
  //       print('not granted');
  //       return;
  //     }
  //   }
  // }

  @override
  void initState() {
    // _checkPermissions();
    // _requestPermission();
    _location();
    super.initState();
  }

@override
  void didUpdateWidget(SplashScreenPage oldWidget) {
  // _readDataFromSharedPref();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    print('tttttttt');
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/bg.png'),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Image.asset(
                    'assets/logo.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
