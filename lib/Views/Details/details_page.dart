import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lookin/Models/home_model.dart';
import 'package:lookin/Views/More/terms_conditions_page.dart';
import 'package:lookin/Widgets/standard_button_widget.dart';
import 'package:lookin/Widgets/thanks_sheet.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../Models/details_model.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Views/Details/ratings_page.dart';
import '../../Widgets/button.dart';
import '../../Widgets/details_photo_widget.dart';
import '../../Widgets/text_widgets.dart';
import '../../Generics/NetworkUtil.dart';
import '../../Models/api_response_model.dart';

class DetailsPage extends StatefulWidget {
  static const tag = '/detailsPage';
  final RestaurantsData restaurantData;
  const DetailsPage({this.restaurantData});

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  final _slotNoController = TextEditingController();
  final _noteController = TextEditingController();
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = true;
  DetailsModel _detailsModel;
  List<Branchs> branches;
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  Branchs selectedBranch;
  AppLocalizations appLocalizations;
  bool _visible = false;
  String selectedDate = '';
  String selectedTime = '';
  GoogleMapController mapController;
  List<Marker> _markers = <Marker>[];
  LatLng _center = const LatLng(45.521563, -122.677433);
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    getData();
    super.initState();
  }
  @override
  void dispose() {
_slotNoController.dispose();
_noteController.dispose();
    super.dispose();
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  Future<void> _openMap(double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  _markerDraw() {
    _markers.add(
      Marker(
          markerId: MarkerId('SomeId'),
          position: LatLng(branches.first.latitude, branches.first.longitude),
          infoWindow: InfoWindow(title: widget.restaurantData.name),
          onTap: () {
            _openMap(_center.latitude, _center.longitude);
          }),
    );
    _center = LatLng(branches.first.latitude, branches.first.longitude);
  }

  void visibility() {
    _visible = !_visible;
  }

  void _showSheet(BuildContext ctx, Widget sheet, bool isDismissable) {
    showModalBottomSheet<dynamic>(
      isDismissible: isDismissable,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      context: ctx,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Wrap(
            children: <Widget>[
              sheet,
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: true,
        key: scaffoldKey,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: primaryColorLight,
          title: MyText.mTextNLBold(
              context: context,
              text: widget.restaurantData.name,
              color: Colors.black),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.share),
                color: primaryColor,
                iconSize: 35,
                onPressed: () {
                  Share.share('You are invited to usae lookin');
                }),
          ],
        ),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Padding(
              padding:  padding,
              child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    DetailsPhotoWidget(
                      _detailsModel,
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(_detailsModel.desc)),
                    Container(
                      margin: const EdgeInsets.all(8.0),
                      height: 150,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: GoogleMap(
                          markers: Set<Marker>.of(_markers),
                          onMapCreated: _onMapCreated,
                          initialCameraPosition: CameraPosition(
                            target: _center,
                            zoom: 11.0,
                          ),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          width: 1,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 60,
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(horizontal: 2),
                              onTap: () {
                                setState(() {
                                  visibility();
                                });
                              },
                              title: MyText.sTextBold(
                                context: context,
                                text: 'Branches',
                                color: Colors.black,
                              ),
                              trailing: Icon(
                                _visible
                                    ? Icons.arrow_drop_up
                                    : Icons.arrow_drop_down,
                                size: 30,
                              ),
                            ),
                          ),
                          Visibility(
                            visible: _visible,
                            child: ListView.separated(
                                separatorBuilder: (context, index) => Divider(
                                      endIndent: 10,
                                      indent: 10,
                                      color: accentColor,
                                    ),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: _detailsModel.branchs.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    child: ListTile(
                                      title: MyText.sTextNL(
                                          context: context,
                                          text: _detailsModel.name /////////
                                              .toString(),
                                          color: Colors.black),
                                      subtitle: MyText.sTextNL(
                                          context: context,
                                          text: _detailsModel
                                              .branchs[index].address
                                              .toString(),
                                          color: Colors.black),
                                    ),
                                  );
                                }),
                          )
                        ],
                      ),
                      margin: const EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          width: 1,
                        ),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 2, vertical: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: MyText.sTextBold(
                                  context: context,
                                  text: 'terms_conditions',
                                  color: Colors.black),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  _detailsModel.desc.length < 10 ||
                                          _detailsModel.desc.length == null
                                      ? Text('')
                                      : Text(_detailsModel.desc.substring(0, 10) +
                                          '...'),
                                  InkWell(
                                    onTap: () {
                                      Navigator.of(context)
                                          .pushNamed(TermsConditionsPage.tag);
                                    },
                                    child: MyText.sText(
                                        context: context,
                                        text: 'See_More',
                                        color: primaryColor),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      margin: const EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          width: 1,
                        ),
                      ),
                    ),
                    _detailsModel.rate == null ? Container() : _buildRating(),
                    constUserLoginModel.name == ''
                        ? Container(
                            margin: EdgeInsets.all(20),
                            width: hBlock * 85,
                            height: 50,
                            child: RaisedButton(
                                color: accentColor,
                                elevation: 2,
                                onPressed: () {
                                  scaffoldKey.currentState.showSnackBar(
                                    SnackBar(
                                      content:
                                          //Text("Wrong email or password"),
                                          Text(
                                        "Please login or sign up",
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  );
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: MyText.mText(
                                  context: context,
                                  text: 'Book_Your_Place_Now',
                                  color: Colors.white,
                                )),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Btn.button(
                                context: context,
                                title: 'Book_Your_Place_Now',
                                onPressed: () {
                                  setState(() {
                                    _showSheet(context, _bookingSheet(), true);
                                  });
                                }),
                          ),
                  ],
                ),
            ),
      ),
    );
  }

  Widget _buildRating() {
    return Container(
      margin: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          width: 1,
        ),
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                MyText.sTextBold(
                    context: context, text: 'Ratings', color: Colors.black),
                _detailsModel.rateCount > 1
                    ? InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RatingsPage(
                                      id: _detailsModel.id,
                                    )),
                          );
                        },
                        child: MyText.sText(
                            context: context,
                            text: 'See_More',
                            color: primaryColor),
                      )
                    : Container(),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 25,
                  backgroundColor: Colors.transparent,
                  child: ClipOval(
                    child: _detailsModel.rate.user.image != null
                        ? CachedNetworkImage(
                            height: 50,
                            width: 50,
                            imageUrl: _detailsModel.rate.user.image,
                            placeholder: (context, url) =>
                                Center(child: CircularProgressIndicator()),
                            fit: BoxFit.cover,
                            errorWidget: (context, url, error) => Image.asset(
                              'assets/person.png',
                              fit: BoxFit.cover,
                            ),
                          )
                        : Image.asset(
                            'assets/person.png',
                            fit: BoxFit.fitWidth,
                          ),
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MyText.sTextNLBold(text: _detailsModel.rate.user.name),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: ratingStars((_detailsModel.rate.rate.toDouble())),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(_detailsModel.rate.comment),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget ratingStars(double rate) {
    //var rating = 0.0;
    return StatefulBuilder(builder: (BuildContext context, setState) {
      return IgnorePointer(
        child: RatingBar(
          initialRating: rate, // provider.getStars.toDouble(),,
          itemSize: hBlock * 6,
          itemCount: 5,
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: primaryColor,
          ),
          unratedColor: Colors.grey,
          textDirection: TextDirection.rtl,
          allowHalfRating: true,
          onRatingUpdate: (value) {},
        ),
      );
    });
  }

  _showDateTimePopUp() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Directionality(
          textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
          child: SimpleDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10),
                child: Center(
                  child:
                      MyText.sTextBold(context: context, text: 'Select_time'),
                ),
              ),
              Container(
                // margin: const EdgeInsets.all(5),
                height: 150,
                // width: hBlock * 95,
                child: CupertinoDatePicker(
                  use24hFormat: false,
                  mode: CupertinoDatePickerMode.dateAndTime,
                  initialDateTime: DateTime.now(),
                  onDateTimeChanged: (DateTime dateTime) {
                    print("dateTime: $dateTime");
                    String stringifiedDatetime = dateTime.toString();
                    selectedDate = stringifiedDatetime.substring(0, 10);
                    selectedTime = stringifiedDatetime.substring(11, 16);
                    print(selectedDate);
                    print(selectedTime);
                  },
                  minuteInterval: 1,
                ),
              ),
              Btn.button(
                  context: context,
                  title: 'Done',
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).pop();
                      _showSheet(context, _bookingSheet(), true);
                    });
                  })
            ],
          ),
        );
      },
    );
  }

  Widget _bookingSheet() {
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SingleChildScrollView(
        padding: padding,
        child: Container(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Container(
                      height: 5,
                      width: 50,
                      color: accentColor,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                      child: MyText.mTextBold(
                          context: context, text: 'Book_Your_Place_Now')),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: MyText.sText(
                      context: context,
                      text: "This_is_nearst_branch_you_can_change",
                      color: accentColor),
                ),
                ListTile(
                    contentPadding: EdgeInsets.all(2),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        MyText.mTextNL(
                          context: context,
                          text: _detailsModel.name,
                        ),
                        MyText.sTextNL(
                          context: context,
                          text: selectedBranch.address,
                        ),
                      ],
                    ),
                    trailing: Icon(Icons.arrow_forward_ios),
                    onTap: () {
                      Navigator.of(context).pop();
                      branches.forEach((branch) {
                        branch.isSelected = false;
                      });
                      _showSheet(context, _branchSheet(), false);
                    }),
                Container(
                  height: 1,
                  width: double.infinity,
                  color: accentColor,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: MyText.sText(
                      context: context,
                      text: "Select_time",
                      color: accentColor),
                ),
                ListTile(
                    contentPadding: EdgeInsets.all(2),
                    title: selectedDate == ''
                        ? MyText.sText(
                            context: context, text: 'Select_suitable_date')
                        : MyText.sTextNL(
                            context: context,
                            text: selectedDate + '      ' + selectedTime),
                    trailing: Icon(Icons.arrow_forward_ios),
                    onTap: () {
                      Navigator.of(context).pop();
                      _showDateTimePopUp();
                    }),
                Container(
                  height: 1,
                  width: double.infinity,
                  color: accentColor,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: MyText.sText(
                      context: context,
                      text: "No_of_persons",
                      color: accentColor),
                ),
                Container(
                  height: 40,
                  child: TextFormField(
                    maxLines: 2,
                    decoration: InputDecoration(
                        hintText: AppLocalizations.of(context)
                            .getMessageByLangAndKey(
                                appLanguageCode, 'Write_no_of_persons')),
                    controller: _slotNoController,
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value.isEmpty) {
                        _btnController.reset();
                        return appLocalizations.getMessageByLangAndKey(
                            appLanguageCode, 'Write_no_of_persons');
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: MyText.sText(
                      context: context, text: "Notes", color: accentColor),
                ),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: AppLocalizations.of(context)
                          .getMessageByLangAndKey(
                              appLanguageCode, 'Write_your_notes')),
                  maxLines: 3,
                  controller: _noteController,
                  validator: (value) {
                    if (value.isEmpty) {
                      _btnController.reset();
                      return appLocalizations.getMessageByLangAndKey(
                          appLanguageCode, 'Write_your_notes');
                    } else {
                      return null;
                    }
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: RoundedLoadingButton(
                    color: primaryColor,
                    width: hBlock * 85,
                    controller: _btnController,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        postBooking();
                      }
                      print(DateTime.now());
                    },
                    child: MyText.mText(
                        context: context,
                        text: "Book_Now",
                        color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //TODO return selection to booking sheet
  Widget _branchSheet() {
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return SingleChildScrollView(
          padding: padding,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Center(
                  child: Container(
                    height: 5,
                    width: 50,
                    color: accentColor,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      onPressed: () {
                        Navigator.of(context).pop();
                        _showSheet(context, _bookingSheet(), true);
                      },
                    ),
                    MyText.mTextBold(context: context, text: 'Branches'),
                    SizedBox(
                      width: 46,
                    ),
                  ],
                ),
              ),
              ListView.builder(
                scrollDirection: Axis.vertical,
                physics: ScrollPhysics(),
                shrinkWrap: true,
                itemCount: branches.length,
                itemBuilder: (context, index) {
                  return _buildBranchItem(context, index, setState);
                },
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _buildBranchItem(
      BuildContext context, int index, StateSetter setState) {
    return Column(
      children: <Widget>[
        ListTile(
          onTap: () {
            setState(() {
              branches.forEach((branch) {
                branch.isSelected = false;
              });
              branches[index].isSelected = !branches[index].isSelected;
              selectedBranch = branches[index];
              print('sssssss' + branches[index].isSelected.toString());
            });
          },
          title: MyText.mTextNL(context: context, text: _detailsModel.name),
          subtitle:
              MyText.sTextNL(context: context, text: branches[index].address),
          // trailing: Container(
          //     color: branches[index].isSelected ? primaryColor : accentColor,
          //     height: 30,
          //     width: 30,
          //     ),
          trailing: branches[index].isSelected
              ? Container(
                  color: primaryColor,
                  height: 30,
                  width: 30,
                  child: Icon(
                    Icons.check,
                    color: canvusColor,
                  ),
                )
              : Container(
                  color: accentColor,
                  height: 30,
                  width: 30,
                ),
        ),
        Container(
          margin: EdgeInsets.all(8),
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }

  getData() async {
    Map<String, dynamic> jsonMap = {
      "longitude": constLong,
      "latitude": constLat,
    };

    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'store/${widget.restaurantData.id}',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      _detailsModel = DetailsModel.fromJson(responseModel.data);
      branches = _detailsModel.branchs;
      selectedBranch = branches[0];
      String stringifiedDatetime = DateTime.now().toString();
      selectedDate = stringifiedDatetime.substring(0, 10);
      selectedTime = stringifiedDatetime.substring(11, 16);

      if (_detailsModel == null) {
        return;
      }
      if (mounted) {
        setState(() {
          _markerDraw();
          _isLoading = false;
        });
      }
    } else {
      _isLoading = true;
    }
  }

  postBooking() async {
    Map<String, dynamic> jsonMap = {
      "time": selectedTime,
      "date": selectedDate,
      "slot_no": int.parse(_slotNoController.text),
      "main_store_id": widget.restaurantData.id,
      "store_id": selectedBranch.id,
      "note": _noteController.text
    };
    String jsonString = json.encode(jsonMap);

    print(jsonString);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'bookings',
      jsonString,
    );
    if (responseModel == null) {
      print(responseModel.statuscode);
      return;
    }
    if (responseModel.statuscode == 200) {
      print('responseModel.message: ' + responseModel.message);
      _btnController.success();
      _noteController.clear();
      _slotNoController.clear();
      setState(() {
        Navigator.of(context).pop();
        _showSheet(
            context,
            ThanksSheet(
              title: 'Successfully_Reserved',
              message: 'Resposible_Reply',
              buttonTitle: 'Follow_Your_Request',
              function: () {
                //TODO: to redirct to requests page
                setState(() {
                  currentbarIndex = 1;
                  Navigator.pop(context);
                  // Navigator.of(context).pop();
                  // Navigator.pushAndRemoveUntil(
                  //     context,
                  //     MaterialPageRoute(builder: (context) => Dashboard()),
                  //     (Route<dynamic> route) => false);
                });
              },
            ),
            true);
      });
    } else {
      _btnController.reset();
    }
  }
}
