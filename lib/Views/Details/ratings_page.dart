import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lookin/Widgets/user_photo.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import '../../Widgets/text_widgets.dart';
import '../../Generics/NetworkUtil.dart';
import '../../Models/api_response_model.dart';
import '../../Models/ratings_model.dart';

class RatingsPage extends StatefulWidget {
  static const tag = '/RatingsPage';
  final int id;
  const RatingsPage({this.id});
  @override
  _RatingsPageState createState() => _RatingsPageState();
}

class _RatingsPageState extends State<RatingsPage> {
  AppLocalizations appLocalizations;
  bool _isLoading = true;
  List<RatingDataModel> _ratingList = [];
  int _pageAll = 1;
  int _lastPage = 1;


  Future<void> _refreshRatingData() async {
    print(' I am refreshing data');
    if (_pageAll > _lastPage) {
      _isLoading = false;
      return;
    } else {
      setState(() {
        _getData();
      });
    }
  }

  @override
  void initState() {
    _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: canvusColor,
          title: MyText.mTextBold(
              context: context, text: "Ratings", color: Colors.black),
          centerTitle: true,
        ),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Padding(
              padding: padding,
              child: LazyLoadScrollView(
                  scrollOffset: 0,
                  onEndOfPage: _refreshRatingData,
                  child: RefreshIndicator(
                    onRefresh: _refreshRatingData,
                    child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: _ratingList.length,
                      itemBuilder: (context, index) {
                        return _buildRating(index);
                      },
                    ),
                  ),
                ),
            ),
      ),
    );
  }

  Widget _buildRating(int index) {
    return Container(
      height: 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(height: 1, color: Colors.transparent),
          Row(
            children: <Widget>[
              UserPhoto(imageUrl: _ratingList[index].user.image, height: 50),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(_ratingList[index].user.name),
                  ratingStars((_ratingList[index].rate.toDouble())),
                  Text(_ratingList[index].comment)
                ],
              ),
            ],
          ),
          Container(height: 1, color: accentColor)
        ],
      ),
    );
  }

  Widget ratingStars(double rate) {
    return StatefulBuilder(builder: (BuildContext context, setState) {
      return IgnorePointer(
        child: RatingBar(
          initialRating: rate,
          itemSize: hBlock * 6,
          itemCount: 5,
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: primaryColor,
          ),
          unratedColor: Colors.grey,
          textDirection: TextDirection.rtl,
          allowHalfRating: true,
          onRatingUpdate: (value) {},
        ),
      );
    });
  }

  _getData() async {
    ApiResponseModel responseModel =
        await NetworkUtil.getWithoutToken('rates/${widget.id}?page=$_pageAll');
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      RatingsModel model = RatingsModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _lastPage = model.lastPage;
      _pageAll++;
      _ratingList.addAll(model.data);
      print(_ratingList.length);
      _ratingList.map((f) {
        print(f.rate);
      });
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = true;
    }
  }
}
