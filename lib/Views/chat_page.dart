import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Models/inbox_messages_model.dart';
import 'package:lookin/PreDefined/app_constants.dart';
import 'package:lookin/PreDefined/localization.dart';
import 'package:lookin/Utils/size_config.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/text_widgets.dart';

class ChatPage extends StatefulWidget {
  final String title;
  final int id;
  static const tag = '/ChatPage';

  const ChatPage({this.title, this.id});
  @override
  State createState() => ChatPageState();
}

class ChatPageState extends State<ChatPage> with TickerProviderStateMixin {
  final List<ChatMessage> _messages = <ChatMessage>[];
  final TextEditingController _textController = TextEditingController();
  bool _isComposing = false;
  AppLocalizations appLocalizations;
  bool _isLoading = false;
  Booking _booking;
  List<Inbox> _inbox = [];

  @override
  void initState() {
    getData();
    super.initState();
  }

  void _handleSubmitted(String text) {
    _textController.clear();
    setState(() {
      _isComposing = false;
    });
    Inbox message = Inbox(
      id: _booking.id,
      msg: text,
      senderType: 2,
      date: DateTime.now().millisecondsSinceEpoch,
      animationController: AnimationController(
        duration: Duration(milliseconds: 700),
        vsync: this,
      ),
    );
    setState(() {
      _inbox.insert(0, message);
    });
    message.animationController.forward();
  }

  void dispose() {
    for (ChatMessage message in _messages)
      message.animationController.dispose();
      _textController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBarNL(context, widget.title),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Container(
               padding: padding,
                child: Column(children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 8, left: 8, right: 8),
                    padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                    height: vBlock * 25,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(
                        color: accentColor,
                        width: 1,
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        MyText.mTextBold(
                            context: context, text: 'Your_order_details'),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            MyText.sText(
                              context: context,
                              text: 'Booking_Date',
                            ),
                            Text(_booking.date),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            MyText.sText(
                              context: context,
                              text: 'Time',
                            ),
                            Text(_booking.time),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            MyText.sText(
                              context: context,
                              text: 'No_Persons',
                            ),
                            Text(_booking.slotNo.toString()),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            MyText.sText(
                              context: context,
                              text: 'Request_Status',
                            ),
                            Text(
                              _booking.status == 1
                                  ? appLocalizations.getMessageByLangAndKey(
                                      appLanguageCode, 'Waiting_Confirmation')
                                  : _booking.status == 2
                                      ? appLocalizations.getMessageByLangAndKey(
                                          appLanguageCode, 'Confirmed')
                                      : _booking.status == 3
                                          ? appLocalizations
                                              .getMessageByLangAndKey(
                                                  appLanguageCode, 'attended')
                                          : _booking.status == 4
                                              ? appLocalizations
                                                  .getMessageByLangAndKey(
                                                      appLanguageCode,
                                                      'Did_not_attend')
                                              : _booking.status == 5
                                                  ? appLocalizations
                                                      .getMessageByLangAndKey(
                                                          appLanguageCode,
                                                          'client_canceled')
                                                  : appLocalizations
                                                      .getMessageByLangAndKey(
                                                          appLanguageCode,
                                                          'Rejected'),
                              style: TextStyle(
                                color: _booking.status == 1
                                    ? Colors.orange
                                    : _booking.status == 2
                                        ? primaryColor
                                        : _booking.status == 3
                                            ? primaryColor
                                            : _booking.status == 4
                                                ? Colors.red
                                                : _booking.status == 5
                                                    ? Colors.red
                                                    : Colors.red,
                              ),
                            ),
                          ],
                        ),
                        // list[index].status == 4
                        //     ? Container(
                        //         height: 0,
                        //       )
                        //     :
                      ],
                    ),
                  ),
                  Flexible(
                      child: ListView.builder(
                    reverse: true,
                    itemBuilder: (_, int index) => Row(
                      // mainAxisAlignment: _inbox[index].senderType == 2
                      //     ? MainAxisAlignment.start
                      //     : MainAxisAlignment.end,
                      children: <Widget>[
                        Flexible(child: ChatMessage(message: _inbox[index])),
                      ],
                    ),
                    itemCount: _inbox.length,
                  )),
                  Divider(height: 1.0),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    decoration:
                        BoxDecoration(color: Theme.of(context).cardColor),
                    child: _buildTextComposer(),
                  ),
                ]),
                decoration: Theme.of(context).platform == TargetPlatform.iOS
                    ? BoxDecoration(
                        border:
                            Border(top: BorderSide(color: Colors.grey[200])))
                    : null), //
      ),
    );
  }

  Widget _buildTextComposer() {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Column(
        children: <Widget>[
          Container(
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      cursorColor: primaryColor,
                      keyboardType: TextInputType.multiline,
                      controller: _textController,
                      onChanged: (String text) {
                        setState(() {
                          _isComposing = text.length > 0;
                        });
                      },
                      //onSubmitted: _handleSubmitted,
                      decoration: InputDecoration.collapsed(
                          hintText: appLocalizations.getMessageByLangAndKey(
                              appLanguageCode, 'Write_your_message')),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 4.0),
                    child: IconButton(
                      icon: Icon(Icons.send,
                          color: _isComposing ? primaryColor : accentColor),
                      onPressed: _isComposing && _textController.text != null
                          ? () {
                              postMessage();
                              _handleSubmitted(_textController.text);
                            }
                          : null,
                    ),
                  ),
                ],
              ),
              decoration: Theme.of(context).platform == TargetPlatform.iOS
                  ? BoxDecoration(
                      border: Border(top: BorderSide(color: Colors.grey[200])))
                  : null),
          SizedBox(height: 20)
        ],
      ),
    );
  }

  getData() async {
    _isLoading = true;
    ApiResponseModel responseModel = await NetworkUtil.get(
      'inbox/${widget.id}',
    );
    print(widget.id);
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      //Restaurants model = Restaurants.fromJson(responseModel.data);
      InboxMessagesModel model =
          InboxMessagesModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }

      _booking = model.booking;
      _inbox = model.inbox;

      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }

  postMessage() async {
    Map<String, dynamic> jsonMap = {
      "msg": _textController.text, /////////////////
      "booking_id": _booking.id
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'inbox',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      print(responseModel.message);
      setState(() {});
    } else {}
  }
}

class ChatMessage extends StatefulWidget {
  ChatMessage({
    this.animationController,
    this.message,
  });
  final Inbox message;
  final AnimationController animationController;

  @override
  _ChatMessageState createState() => _ChatMessageState();
}

class _ChatMessageState extends State<ChatMessage> {
  AppLocalizations appLocalizations;
  @override
  void initState() {
    super.initState();
  }

  _intToDate(int timeInMillis) {
    var datetime = DateTime.fromMillisecondsSinceEpoch(timeInMillis);
    String stringifiedDatetime = datetime.toString();
    var selectedDate = stringifiedDatetime.substring(0, 10);
    return selectedDate;
  }

  _intToTime(int timeInMillis) {
    var datetime = DateTime.fromMillisecondsSinceEpoch(timeInMillis);
    String stringifiedDatetime = datetime.toString();
    var selectedTime = stringifiedDatetime.substring(11, 16);
    return selectedTime;
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        crossAxisAlignment:
            isRightToLeft ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: widget.message.senderType == 1
                  ? CrossAxisAlignment.end
                  : CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(8.0),
                  child: Container(
                    width: hBlock * 70,
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(
                        color: accentColor,
                        width: 1,
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        MyText.mTextNL(
                          context: context,
                          text: widget.message.msg,
                          color: widget.message.senderType == 1
                              ? accentColor
                              : primaryColor,
                        ),
                        SizedBox(height: 5),
                        MyText.xsTextNL(
                          context: context,
                          text: _intToTime(widget.message.date).toString() +
                              '     ' +
                              _intToDate(widget.message.date).toString(),
                          color: widget.message.senderType == 1
                              ? accentColor
                              : primaryColor,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
