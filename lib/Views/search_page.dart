import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/no_data_widget.dart';
import '../Models/home_model.dart';
import '../PreDefined/app_constants.dart';
import '../PreDefined/localization.dart';
import '../Utils/size_config.dart';
import '../Widgets/home_photo_widget.dart';

import '../Generics/NetworkUtil.dart';
import '../Models/api_response_model.dart';
import 'Details/details_page.dart';

class SearchPage extends StatefulWidget {
  static const String tag = '/SearchPage';
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  AppLocalizations appLocalizations;
  List<RestaurantsData> _restaurantsData = [];
  final _searchController = TextEditingController();
  bool _isLoading = false;
  int _pageAll = 1;
  int _lastPage = 1;

  Future<void> _refreshAllData() async {
    if (_pageAll > _lastPage) {
      _isLoading = false;
      return;
    } else {
      setState(() {
        print(' I am refreshing data');
        getData(_searchController.text);
      });
    }
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        appBar: CustomAppBar.appBar(context, 'Search', null),
        body: Stack(
          children: <Widget>[
            Positioned(child: _buildSearchTextFieldWidget()),
            Container(
              padding:
                  EdgeInsets.only(top: 70, left: hBlock * 5, right: hBlock * 5),
              child: _isLoading
                  ? Center(child: CircularProgressIndicator())
                  : _restaurantsData.length == 0
                      ? NoDataAvailable(
                          image: 'assets/search_empty.png',
                          text: 'Search_No_Results',
                          visible: false,
                        )
                      : LazyLoadScrollView(
                          scrollOffset: 100,
                          onEndOfPage: _refreshAllData,
                          child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            physics: ScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: _restaurantsData.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => DetailsPage(
                                          restaurantData:
                                              _restaurantsData[index]),
                                    ),
                                  );
                                },
                                child: HomePhotoWidget(_restaurantsData[index]),
                              );
                            },
                          ),
                        ),
            ),
          ],
        ),
      ),
    );
  }

  Container _buildSearchTextFieldWidget() {
    return Container(
      height: 80,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: TextField(
        controller: _searchController,
        textInputAction: TextInputAction.search,
        style: TextStyle(fontSize: hBlock * 4, color: primaryColor),
        // controller: controler,
        autofocus: true,
        //initialValue: initialValue,
        cursorColor: primaryColor,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: accentColor,
              width: 2.0,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: accentColor,
            ),
          ),
          alignLabelWithHint: true,
          prefixIcon: Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
              onPressed: () {
                getData(_searchController.text);
              },
              icon: Icon(
                Icons.search,
                size: 30,
                color: Colors.black,
              ),
            ),
          ),
          fillColor: accentColor,
          hoverColor: accentColor,
          focusColor: accentColor,
          hintText: appLocalizations.getMessageByLangAndKey(
              appLanguageCode, 'Search'),
          hintStyle: TextStyle(fontSize: hBlock * 4, color: accentColor),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          counterStyle: TextStyle(color: primaryColor),
        ),
        onSubmitted: (value) {
          getData(value);
        },
        //validator: ,
      ),
    );
  }

  getData(String val) async {
    _isLoading = true;
    Map<String, dynamic> jsonMap = {
      "text": val,
    };

    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'store/search?page=$_pageAll',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      Restaurants model = Restaurants.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _lastPage = model.lastPage;
      _pageAll++;
      _restaurantsData.addAll(model.data);
      print(_pageAll);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = true;
    }
  }
}
