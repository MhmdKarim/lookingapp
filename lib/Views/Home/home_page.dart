import 'dart:async';
import 'dart:convert';
import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:location/location.dart';
import 'package:lookin/Models/all_filtration_model.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/no_data_widget.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import '../../Generics/NetworkUtil.dart';
import '../../Models/home_model.dart';
import '../../Models/api_response_model.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import '../Filter/filter_page.dart';
import '../Details/details_page.dart';
import 'categories_buttons_widget.dart';
import '../../Widgets/home_photo_widget.dart';
import 'subcategories_buttons_widget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AppLocalizations appLocalizations;
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = true;
  List<Category> _allcategoryList = [];
  List<SubCategory> _allsubcategoryList = [];
  List<RestaurantsData> _restaurantsData = [];
  List<SubCategory> _filteredSubCategory = [];
  int selectedCategoryId = -1;
  int selectedSubCategoryId = -1;
  PermissionStatus _permissionGranted;
  final Location location = Location();

  int _pageAll = 1;
  int _lastPage = 1;

  _location() async {
    bool _serviceEnabled;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();

      print('_serviceEnabled:  ' + _serviceEnabled.toString());
      if (!_serviceEnabled) {
        AppSettings.openLocationSettings();

        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        print('_permissionGranted.not');
        print('goto settings');
        AppSettings.openLocationSettings().then((onValue) {
          print('then on value');
        });
        return;
      }
    }
    locationData = await location.getLocation();
    setState(() {
      constLong = locationData.longitude;
      constLat = locationData.latitude;
      // await Future.delayed(Duration(seconds: 0, milliseconds: 1000));
      getAllData();
    });

    print(locationData.longitude.toString() +
        '     ' +
        locationData.latitude.toString());
  }

  Future<void> _refreshAllData() async {
    print(' I am refreshing data');
    if (_pageAll > _lastPage) {
      _isLoading = false;
      return;
    } else {
      setState(() {
        print('refreshing');
        _location();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _location();
    getCatSubat();
    getAllData();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  _onLogin() {
    setState(() {});
  }

  void _addCategoryId(int id) {
    selectedCategoryId = id;
    print(selectedCategoryId);

    if (selectedCategoryId > -1) {
      setState(() {
        getFilteredData(categoryID: selectedCategoryId, subCategoryId: null);
        _filteredSubCategory = _allsubcategoryList
            .where((subCat) => subCat.id == selectedCategoryId)
            .toList();
        _filteredSubCategory.forEach((subCat) {
          subCat.isSelected = false;
        });
      });
    } else {
      setState(() {
        _filteredSubCategory = _allsubcategoryList;
      });
    }
  }

  void _addSubCategoryId(int id) {
    print(id);
    selectedSubCategoryId = id;
    setState(() {
      getFilteredData(categoryID: selectedCategoryId, subCategoryId: id);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return SafeArea(
      child: Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Scaffold(
                key: scaffoldKey,
                body: Stack(
                  children: <Widget>[
                    Positioned.directional(
                        textDirection: isRightToLeft
                            ? TextDirection.rtl
                            : TextDirection.ltr,
                        top: 0.0,
                        start: 0.0,
                        end: 0.0,
                        child: CustomAppBar.buildAppBarLogin(
                            context, _onLogin, true)),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 80, left: hBlock * 5, right: hBlock * 5),
                      child: LazyLoadScrollView(
                        scrollOffset: 0,
                        onEndOfPage: _refreshAllData,
                        child: RefreshIndicator(
                          onRefresh: _refreshAllData,
                          child: ListView(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 8),
                                    Container(
                                        height: 40,
                                        child: ListView(
                                          scrollDirection: Axis.horizontal,
                                          children: <Widget>[
                                            InkWell(
                                              onTap: () {
                                                print('All');
                                                setState(() {
                                                  getAllData();
                                                  selectedCategoryId = -1;
                                                  _filteredSubCategory.clear();
                                                  // _allcategoryList[selectedIndex].isSelected = false;
                                                  _allcategoryList.forEach((f) {
                                                    f.isSelected = false;
                                                    selectedCategoryId = -1;
                                                  });
                                                });
                                              },
                                              child: Container(
                                                height: 40,
                                                margin: EdgeInsets.only(
                                                    left: hBlock * 2,
                                                    right: hBlock * 2),
                                                // width: hBlock * 12,
                                                child: Padding(
                                                  padding: EdgeInsets.all(
                                                      hBlock * 2),
                                                  child: Center(
                                                    child: Text(
                                                        appLocalizations
                                                            .getMessageByLangAndKey(
                                                                appLanguageCode,
                                                                'All'),
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: selectedCategoryId >
                                                                    -1
                                                                ? accentColor
                                                                : canvusColor,
                                                            //fontWeight: FontWeight.bold,
                                                            fontSize: sFont)),
                                                  ),
                                                ),
                                                decoration: BoxDecoration(
                                                  color: selectedCategoryId > -1
                                                      ? canvusColor
                                                      : primaryColor,
                                                  border: Border.all(
                                                      width: 1.0,
                                                      color:
                                                          selectedCategoryId >
                                                                  -1
                                                              ? accentColor
                                                              : primaryColor),
                                                  borderRadius:
                                                      const BorderRadius.all(
                                                          const Radius.circular(
                                                              10.0)),
                                                ),
                                              ),
                                            ),
                                            Container(
                                                width: hBlock * 80,
                                                child: CategoriesButtons(
                                                    _allcategoryList,
                                                    _addCategoryId)),
                                          ],
                                        )),
                                    SizedBox(height: 8),
                                    Container(
                                        height: 40,
                                        child: ListView(
                                          scrollDirection: Axis.horizontal,
                                          children: <Widget>[
                                            Container(
                                              // width: hBlock * 12,
                                              child: IconButton(
                                                  iconSize: hBlock * 10,
                                                  icon: Icon(
                                                    MdiIcons.filter,
                                                    color: primaryColor,
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(context)
                                                        .pushNamed(
                                                            FilterPage.tag);
                                                  }),
                                            ),
                                            InkWell(
                                              onTap: () {
                                                getFilteredData(
                                                    categoryID:
                                                        selectedCategoryId,
                                                    subCategoryId: null);
                                                selectedSubCategoryId = -1;
                                                _allsubcategoryList
                                                    .forEach((f) {
                                                  f.isSelected = false;
                                                });
                                              },
                                              child: Container(
                                                height: 40.0,
                                                width: hBlock * 12,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Center(
                                                    child: Text(
                                                        appLocalizations
                                                            .getMessageByLangAndKey(
                                                                appLanguageCode,
                                                                'All'),
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: selectedCategoryId >
                                                                        -1 &&
                                                                    selectedSubCategoryId >
                                                                        -1
                                                                ? accentColor
                                                                : primaryColor,
                                                            // fontWeight: FontWeight.bold,
                                                            fontSize: sFont)),
                                                  ),
                                                ),
                                                decoration: BoxDecoration(
                                                  border: Border(
                                                    bottom: BorderSide(
                                                        width: 2.0,
                                                        color: selectedCategoryId >
                                                                    -1 &&
                                                                selectedSubCategoryId >
                                                                    -1
                                                            ? accentColor
                                                            : primaryColor),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              width: hBlock * 75,
                                              child: SubCategoriesButtons(
                                                  _filteredSubCategory,
                                                  _addSubCategoryId),
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                child: _restaurantsData.length == 0
                                    ? NoDataAvailable(
                                        image: 'assets/home-in.png',
                                        text: 'home_is_empty',
                                        visible: true,
                                        onPressed: _refreshAllData,
                                      )
                                    : ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        physics: ScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: _restaurantsData.length,
                                        itemBuilder: (context, index) {
                                          return InkWell(
                                            onTap: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailsPage(
                                                          restaurantData:
                                                              _restaurantsData[
                                                                  index]),
                                                ),
                                              );
                                            },
                                            child: HomePhotoWidget(
                                                _restaurantsData[index]),
                                          );
                                        },
                                      ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  getAllData() async {
    Map<String, dynamic> jsonMap = {
      "longitude": constLong, // "46.92847",
      "latitude": constLat, // "24.476945",
    };

    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = constUserLoginModel.id == 0
        ? await NetworkUtil.postWithoutToken(
            'home?page=$_pageAll',
            jsonString,
          )
        : await NetworkUtil.post(
            'home?page=$_pageAll',
            jsonString,
          );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      HomeModel homeModel = HomeModel.fromJson(responseModel.data);
      if (homeModel == null) {
        return;
      }
      if (homeModel.categories == null) {
        return;
      }
      _lastPage = homeModel.restaurants.lastPage;
      _pageAll++;
      _restaurantsData.addAll(homeModel.restaurants.data);

      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }

  getCatSubat() async {
    ApiResponseModel responseModel = await NetworkUtil.getWithoutToken(
      'dataFilter',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      AllFiltrationModel model =
          AllFiltrationModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _allcategoryList = model.categories;
      _allsubcategoryList = model.subCategories;
      setState(() {
        _isLoading = false;
      });
    } else {
      _isLoading = false;
    }
  }

  getFilteredData({int categoryID, int subCategoryId}) async {
    Map<String, dynamic> jsonMap = {
      "longitude": constLong, // "46.92847",
      "latitude": constLat, // "24.476945",
      "category_id": categoryID,
      "sub_category_id": subCategoryId
    };

    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'home',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      Restaurants homeModel = Restaurants.fromJson(responseModel.data);
      if (homeModel == null) {
        return;
      }
      if (homeModel.data == null) {
        return;
      }

      if (mounted) {
        setState(() {
          _restaurantsData = homeModel.data;
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }
}
