import 'package:flutter/material.dart';
import 'package:lookin/Models/all_filtration_model.dart';
import 'package:lookin/PreDefined/app_constants.dart';

class SubCategoriesButtons extends StatefulWidget {
  final List list;
  final Function getID;

  const SubCategoriesButtons(this.list, this.getID);

  @override
  createState() {
    return CategoriesButtonsState();
  }
}

class CategoriesButtonsState extends State<SubCategoriesButtons> {

   int id = 0;
  void _submitData() {
    widget.getID(id);

   // Navigator.of(context).pop();
  }
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 80,
        child: ListView.builder(
          itemCount: widget.list.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                setState(() {
                  widget.list.forEach((element) => element.isSelected = false);
                  widget.list[index].isSelected = true;
                  id = widget.list[index].id;
                  _submitData();
                });
              },
              child: RadioItem(widget.list[index]),
            );
          },
        ),
      ),
    );
  }
}

class RadioItem extends StatelessWidget {
  final SubCategory _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            height: 50.0,
            child: Row(
              children: <Widget>[
                Text(_item.name,
                    style: TextStyle(
                        color: _item.isSelected ? primaryColor : accentColor,
                        //fontWeight: FontWeight.bold,
                        fontSize: sFont)),
              ],
            ),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                    width: 2.0,
                    color: _item.isSelected ? primaryColor : accentColor),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
