import 'package:flutter/material.dart';
import 'package:lookin/Models/all_filtration_model.dart';
import 'package:lookin/PreDefined/app_constants.dart';

class CategoriesButtons extends StatefulWidget {
  final List list;
  final Function getID;

  const CategoriesButtons(this.list, this.getID);
  @override
  createState() {
    return CategoriesButtonsState();
  }
}

class CategoriesButtonsState extends State<CategoriesButtons> {
  int id = 0;
  // int _index;
  void _submitData() {
    widget.getID(id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView.builder(
          itemCount: widget.list.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                setState(() {
                  widget.list.forEach((element) => element.isSelected = false);
                  widget.list[index].isSelected = true;
                  id = widget.list[index].id;
                  // _index = index;
                });
                _submitData();
              },
              child: ButtonItem(widget.list[index]),
            );
          },
        ),
      ),
    );
  }
}

class ButtonItem extends StatelessWidget {
  final Category _item;
  ButtonItem(this._item);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: hBlock * 2, right: hBlock * 2),
      child: Container(
        child: Padding(
          padding: EdgeInsets.all(hBlock * 2),
          child: Center(
            child: Text(_item.name.toString(),
                style: TextStyle(
                    color: _item.isSelected ? canvusColor : accentColor,
                    //fontWeight: FontWeight.bold,
                    fontSize: sFont)),
          ),
        ),
        decoration: BoxDecoration(
          color: _item.isSelected ? primaryColor : Colors.transparent,
          border: Border.all(
              width: 1.0, color: _item.isSelected ? primaryColor : accentColor),
          borderRadius: const BorderRadius.all(const Radius.circular(10.0)),
        ),
      ),
    );
  }
}
