import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import '../../Generics/NetworkUtil.dart';
import '../../Models/login_model.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Views/AuthBottomSheets/registration_sheet.dart';
import '../../Widgets/button.dart';
import '../../Widgets/text_widgets.dart';
import '../../Generics/app_prefs.dart';
import '../../Models/api_response_model.dart';

class LoginSheet extends StatefulWidget {
  final Function onLogin;
  final BuildContext context;
  const LoginSheet(this.onLogin, this.context);
  @override
  _LoginSheetState createState() {
    return _LoginSheetState();
  }
}

class _LoginSheetState extends State<LoginSheet> {
  final _passwordController = TextEditingController();
  TextEditingController _controller = TextEditingController();
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  AppLocalizations appLocalizations;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String message = '';
  bool isVisible = false;
  UserModel loginModel;
  bool _isFormvalid = false;
  PhoneNumber _number;
  String _responseMessage = '';
  bool _showMessage = false;
  String _initialCode = 'EG';
  PhoneNumber _initialPhoneNumber;

  @override
  void initState() {
        _initialCode = Platform.localeName.split('_')[1];
    print(_initialCode);
    _initialPhoneNumber =
        PhoneNumber(isoCode: _initialCode == null ? 'EG' : _initialCode);
    super.initState();
  }

  @override
  void dispose() {
_passwordController.dispose();
_controller.dispose();

    super.dispose();
  }

  _onRegistration() {
    widget.onLogin();
  }

  void _showRegistrationSheet(BuildContext ctx) {
    showModalBottomSheet<dynamic>(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      context: ctx,
      builder: (context) {
        return Wrap(
          children: <Widget>[
            RegistrationSheet(_onRegistration),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);
    return Builder(
      builder: (context) {
        return Directionality(
          textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                top: 10,
                left: hBlock * 5,
                right: hBlock * 5,
                bottom: MediaQuery.of(context).viewInsets.bottom + 20,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Container(
                        height: 5,
                        width: 50,
                        color: accentColor,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: MyText.lTextBold(
                        context: context,
                        text: 'Login',
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: MyText.mText(
                        context: context,
                        text: 'Please_Login_With_Phone',
                      ),
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              InternationalPhoneNumberInput(
                                initialValue: _initialPhoneNumber,
                                hintText:
                                    appLocalizations.getMessageByLangAndKey(
                                        appLanguageCode, 'Phone_Number'),
                                errorMessage:
                                    appLocalizations.getMessageByLangAndKey(
                                        appLanguageCode, 'wrong_phone_number'),
                                onInputChanged: (PhoneNumber number) {
                                  print(number.phoneNumber);
                                  _number = number;
                                },
                                onInputValidated: (bool value) {
                                  print('vvvvv' + value.toString());
                                },
                                ignoreBlank: true,
                                autoValidate: true,
                                textFieldController: _controller,
                                selectorType: PhoneInputSelectorType.DIALOG,
                              ),
                            ],
                          ),
                        ),
                        TextFormField(
                          obscureText: true,
                          decoration: InputDecoration(
                            labelText: appLocalizations.getMessageByLangAndKey(
                                appLanguageCode, 'Password'),
                          ),
                          controller: _passwordController,
                          autovalidate: _isFormvalid,
                          validator: (value) {
                            if (value.isEmpty) {
                              // _btnController.reset();
                              return appLocalizations.getMessageByLangAndKey(
                                      appLanguageCode, 'Enter') +
                                  ' ' +
                                  appLocalizations.getMessageByLangAndKey(
                                      appLanguageCode, 'Password');
                            } else {
                              return null;
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: _showMessage,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Center(
                          child: MyText.sTextNL(
                              context: context,
                              text: _responseMessage,
                              color: Colors.red)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 30),
                    child: RoundedLoadingButton(
                      color: primaryColor,
                      width: hBlock * 85,
                      controller: _btnController,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _isFormvalid = false;
                          _postLogin();
                        } else {
                          _isFormvalid = true;
                          _btnController.reset();
                          print('form is invalid');
                        }
                      },
                      child: MyText.mText(
                          context: context, text: 'Login', color: Colors.white),
                    ),
                  ),
                  Center(
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                        _showRegistrationSheet(context);
                      },
                      child: MyText.mText(
                        context: context,
                        text: 'Create_New_Account',
                        color: primaryColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 20)
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  _postLogin() async {
    Map<String, dynamic> jsonMap = {
      "phone": _number.phoneNumber,
      "password": _passwordController.text,
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'login',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    print(responseModel.message);
    _responseMessage = responseModel.message;
    if (responseModel.statuscode == 200) {
      UserModel model = UserModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      String loginModel = json.encode(model);
      savePrefs('loginModel', loginModel);
      savePrefsbool('isLoggedIn', true);
      constUserLoginModel = model;
      isLoggedIn = true;
      _btnController.success();
      widget.onLogin();
      Navigator.of(context).pop();
      // Scaffold.of(widget.context).showSnackBar(SnackBar(
      //     backgroundColor: primaryColor,
      //     duration: Duration(seconds: 2),
      //     content: Text(
      //       responseModel.message,
      //       textAlign: TextAlign.center,
      //     )));
    } else {
      _btnController.reset();
      setState(() {
        _showMessage = true;
      });
    }
  }
}
