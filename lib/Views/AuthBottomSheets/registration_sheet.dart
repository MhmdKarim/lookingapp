import 'dart:io';
import 'dart:convert';
import 'package:flutter/services.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:lookin/Generics/app_prefs.dart';
import 'package:lookin/Models/login_model.dart';
import 'package:lookin/Utils/authservice.dart';
import 'package:path_provider/path_provider.dart';
import '../../Generics/NetworkUtil.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Widgets/thanks_sheet.dart';
import '../../Widgets/button.dart';
import '../../Widgets/text_widgets.dart';
import '../../Models/api_response_model.dart';

class RegistrationSheet extends StatefulWidget {
  final Function onRegister;

  const RegistrationSheet(
    this.onRegister,
  );
  @override
  _RegistrationSheetState createState() {
    return _RegistrationSheetState();
  }
}

class _RegistrationSheetState extends State<RegistrationSheet> {
  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _formKeyOTP = GlobalKey<FormState>();
  File userProfileImage;
  String base64Image;
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  AppLocalizations appLocalizations;
  TextEditingController _controller = TextEditingController();
  String _initialCode = 'US';
  PhoneNumber _initialPhoneNumber;
  PhoneNumber _number;
  String verificationId, smsCode;
  FirebaseUser _userVerifiedPhone;
  bool codeSent = false;
  String _responseMessage = '';
  bool _showMessage = false;

  void _showThanksSheet(BuildContext ctx) {
    showModalBottomSheet<dynamic>(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      context: ctx,
      builder: (context) {
        return Wrap(
          children: <Widget>[
            ThanksSheet(
              title: 'New_Account_Created',
              message: 'Account_Created_Reserve_Easy',
              buttonTitle: 'Thank_You',
              function: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<File> getImageFileFromAssets(String path) async {
    final byteData = await rootBundle.load('assets/$path');

    final file = File('${(await getTemporaryDirectory()).path}/$path');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return file;
  }

  @override
  void initState() {
    _initialCode = Platform.localeName.split('_')[1];
    print(_initialCode);
    _initialPhoneNumber =
        PhoneNumber(isoCode: _initialCode == null ? 'US' : _initialCode);
    getImageFileFromAssets('person.png').then((onValue) {
      userProfileImage = onValue;
    });
    super.initState();
  }
  @override
  void dispose() {
    _controller.dispose();
    _nameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SingleChildScrollView(
        key: _scaffoldKey,
        child: Container(
          padding: EdgeInsets.only(
            top: 10,
            left: hBlock * 5,
            right: hBlock * 5,
            bottom: MediaQuery.of(context).viewInsets.bottom + 10,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Container(
                      height: 5,
                      width: 50,
                      color: accentColor,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: MyText.lTextBold(
                      context: context,
                      text: 'Create_New_Account',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: MyText.mText(
                      context: context,
                      text: 'Sorry_Not_registered',
                    ),
                  ),
                ),
                _imageWidget(),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: appLocalizations.getMessageByLangAndKey(
                        appLanguageCode, 'Name'),
                  ),
                  controller: _nameController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return appLocalizations.getMessageByLangAndKey(
                              appLanguageCode, 'Enter') +
                          ' ' +
                          appLocalizations.getMessageByLangAndKey(
                              appLanguageCode, 'Name');
                    }
                    return null;
                  },
                ),
                IgnorePointer(
                  ignoring: _userVerifiedPhone != null,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InternationalPhoneNumberInput(
                          initialValue: _initialPhoneNumber,
                          isEnabled: _userVerifiedPhone == null,
                          hintText: appLocalizations.getMessageByLangAndKey(
                              appLanguageCode, 'Phone_Number'),
                          errorMessage: appLocalizations.getMessageByLangAndKey(
                              appLanguageCode, 'wrong_phone_number'),
                          onInputChanged: (PhoneNumber number) {
                            print(number.phoneNumber);
                            _number = number;
                          },
                          onInputValidated: (bool value) {
                            print('vvvvv' + value.toString());
                          },
                          ignoreBlank: true,
                          autoValidate: true,
                          textFieldController: _controller,
                          selectorType: PhoneInputSelectorType.DIALOG,
                        ),
                      ],
                    ),
                  ),
                ),
                _userVerifiedPhone != null
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Center(
                            child: MyText.sText(
                                context: context,
                                text: 'Phone_verified',
                                color: primaryColor)),
                      )
                    : Form(
                        key: _formKeyOTP,
                        child: Column(
                          children: <Widget>[
                            codeSent
                                ? Center(
                                    child: Container(
                                      width: hBlock * 50,
                                      //padding: EdgeInsets.only(left: 25.0, right: 25.0),
                                      child: TextFormField(
                                        textAlign: TextAlign.center,
                                        keyboardType: TextInputType.phone,
                                        decoration: InputDecoration(
                                            hintText: appLocalizations
                                                .getMessageByLangAndKey(
                                                    appLanguageCode,
                                                    'Enter_OTP_code')),
                                        onChanged: (val) {
                                          setState(() {
                                            this.smsCode = val;
                                          });
                                        },
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return appLocalizations
                                                    .getMessageByLangAndKey(
                                                        appLanguageCode,
                                                        'Enter') +
                                                ' ' +
                                                appLocalizations
                                                    .getMessageByLangAndKey(
                                                        appLanguageCode,
                                                        'Enter_OTP_code');
                                          }
                                          return null;
                                        },
                                      ),
                                    ),
                                  )
                                : Container(),
                            Center(
                              child: Container(
                                width: hBlock * 50,
                                //padding: EdgeInsets.only(left: 25.0, right: 25.0),
                                child: RaisedButton(
                                  child: codeSent
                                      ? Text('Verify OTP')
                                      : Text('Verify Phone'),
                                  onPressed: () {
                                    if (!_formKeyOTP.currentState.validate()) {
                                      // _btnController.reset();
                                      return;
                                    }
                                    codeSent
                                        ? signInWithOTP(smsCode, verificationId)
                                        : verifyPhone(_number.phoneNumber);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: appLocalizations.getMessageByLangAndKey(
                        appLanguageCode, 'Password'),
                  ),
                  controller: _passwordController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return appLocalizations.getMessageByLangAndKey(
                              appLanguageCode, 'Enter') +
                          ' ' +
                          appLocalizations.getMessageByLangAndKey(
                              appLanguageCode, 'Password');
                    }
                    return null;
                  },
                ),
                Visibility(
                  visible: _showMessage,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Center(
                        child: MyText.sTextNL(
                            context: context,
                            text: _responseMessage,
                            color: Colors.red)),
                  ),
                ),
                Builder(
                  builder: (ctx) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30),
                      child: RoundedLoadingButton(
                        color: primaryColor,
                        width: hBlock * 85,
                        controller: _btnController,
                        onPressed: () {
                          if (!_formKey.currentState.validate() ||
                              _userVerifiedPhone == null) {
                            _btnController.reset();
                            return;
                          }
                          final phone = _number.phoneNumber;
                          print(phone);
                          _postRegister(ctx);
                        },
                        child: MyText.mText(
                            context: context,
                            text: 'Create_New_Account',
                            color: Colors.white),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _imageWidget() {
    return Container(
      //padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Center(
        child: Stack(
          overflow: Overflow.visible,
          fit: StackFit.passthrough,
          //fit: StackFit.loose,
          children: <Widget>[
            ClipRRect(
                child: userProfileImage == null
                    ? CircleAvatar(
                        radius: (hBlock * 25 / 2) + 2,
                        backgroundColor: accentColor,
                        child: CircleAvatar(
                          backgroundColor: accentColor,
                          radius: hBlock * 8,
                          child: Image.asset(
                            'assets/add-image.png',
                            fit: BoxFit.contain,
                            // width: height,
                          ),
                        ),
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            shape: BoxShape.circle),
                        child: CircleAvatar(
                          backgroundImage: FileImage(userProfileImage),
                          backgroundColor: Colors.transparent,
                          maxRadius: 57,
                        ),
                        height: hBlock * 30,
                        width: hBlock * 30,
                      )),
            Positioned(
              top: 0,
              bottom: 0,
              right: -hBlock * 6,
              child: Container(
                height: hBlock * 12,
                width: hBlock * 12,
                decoration: BoxDecoration(
                    border: Border.all(
                        width: 2, color: Theme.of(context).accentColor),
                    color: canvusColor,
                    shape: BoxShape.circle),
                child: IconButton(
                  onPressed: () {
                    _openChooseImagePopup();
                  },
                  icon: Icon(
                    Icons.edit,
                    size: hBlock * 7,
                    color: primaryColor,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _pickImage(ImageSource _imageSource) async {
    final _picker = ImagePicker();

    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
    final File file = File(pickedFile.path);
    List<int> imageBytes = file.readAsBytesSync();
    base64Image = base64Encode(imageBytes);
    if (file != null) {
      setState(() {
        userProfileImage = file;
      });
    }
  }

  _openChooseImagePopup() {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Directionality(
          textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
          child: SimpleDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: MyText.mTextBold(
                context: context, text: "Select_Photo", color: Colors.black),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                  _pickImage(ImageSource.gallery);
                },
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.photo,
                      color: Theme.of(context).primaryColor,
                      size: hBlock * 8,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    MyText.mText(
                        context: context,
                        text: "From_gallery",
                        color: Colors.black),
                  ],
                ),
              ),
              Divider(
                color: Theme.of(context).accentColor,
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                  _pickImage(ImageSource.camera);
                },
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.camera_alt,
                      color: Theme.of(context).primaryColor,
                      size: hBlock * 8,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    MyText.mText(
                        context: context,
                        text: "By_Camera",
                        color: Colors.black),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _postRegister(BuildContext context) async {
    PhoneNumber phoneNumber = await PhoneNumber.getRegionInfoFromPhoneNumber(
        _number.phoneNumber, _number.dialCode);
    print('phoneNumber.phoneNumber' + phoneNumber.phoneNumber);

    Map<String, dynamic> jsonMap = {
      'name': _nameController.text,
      "phone": _userVerifiedPhone.phoneNumber.toString(),
      "password": _passwordController.text,
      'image': base64Image
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'register',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    print(responseModel.message);
    _responseMessage = responseModel.message;
    if (responseModel.statuscode == 200) {
      UserModel model = UserModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }

      String loginModel = json.encode(model);
      print(loginModel);
      savePrefs('loginModel', loginModel);
      savePrefsbool('isLoggedIn', true);
      constUserLoginModel = model;
      isLoggedIn = true;
      _btnController.success();
      widget.onRegister();
      Navigator.of(context).pop();
      _showThanksSheet(context);
    } else {
      _btnController.reset();
      setState(() {
        _showMessage = true;
      });
    }
  }

  Future<void> verifyPhone(phoneNo) async {
    print('verifyPhone');
    final PhoneVerificationCompleted verified = (AuthCredential authResult) {
      AuthService().signIn(authResult);
      print(authResult);
    };

    final PhoneVerificationFailed verificationfailed =
        (AuthException authException) {
      print('${authException.message}');
    };

    final PhoneCodeSent smsSent = (String verId, [int forceResend]) {
      this.verificationId = verId;
      setState(() {
        this.codeSent = true;
      });
    };

    final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {
      this.verificationId = verId;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNo,
        timeout: const Duration(seconds: 0),
        verificationCompleted: verified,
        verificationFailed: verificationfailed,
        codeSent: smsSent,
        codeAutoRetrievalTimeout: autoTimeout);
  }

  signIn(AuthCredential authCreds) async {
    AuthResult result =
        await FirebaseAuth.instance.signInWithCredential(authCreds);
    setState(() {
      _userVerifiedPhone = result.user;
    });
    print('uuuuuu' + _userVerifiedPhone.phoneNumber);
    if (_userVerifiedPhone != null) {
      print('i am verified');
      // _isVerified = true;
    } else {
      print("Error");
    }
  }

  signInWithOTP(smsCode, verId) {
    AuthCredential authCreds = PhoneAuthProvider.getCredential(
        verificationId: verId, smsCode: smsCode);
    signIn(authCreds);
    print('signInWithOTP');
  }
}
