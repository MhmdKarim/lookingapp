import 'package:flutter/material.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/all_filtration_model.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Models/home_filter.dart';
import 'package:lookin/Views/divisions_page.dart';
import 'package:lookin/Widgets/button.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/icon_widget.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import '../../Widgets/text_widgets.dart';

class FilterPage extends StatefulWidget {
  static const tag = '/FilterPage';
  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  bool _isLoading = true;
  AppLocalizations appLocalizations;
  List<Area> _locationChips = [];
  List<Area> _categoryChips = [];
  List<Area> _categoryList = [];
  List<Area> _subcategoryList = [];
  List<Area> _countriesList = [];
  List<Area> _citiesList = [];
  List<Area> _districtList = [];

  void _showSheet(BuildContext ctx, Widget sheet) {
    showModalBottomSheet<dynamic>(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      context: ctx,
      builder: (context) {
        return Wrap(
          children: <Widget>[sheet],
        );
      },
    );
  }

  @override
  void initState() {
    _getFiltrationByData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, 'Filter', null),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Container(
              padding: padding,
                child: ListView(
                  children: <Widget>[
                    _buildListTile(
                        context: context,
                        title: 'Country_City_District',
                        image: 'location-ic',
                        function: () {
                          setState(() {
                            _locationChips.clear();
                            //_locationChipsId.clear();
                          });
                          _showSheet(
                              context,
                              _sheet(
                                  title: 'Country',
                                  length: _countriesList.length,
                                  function: (context, index) {
                                    return _buildCountryItem(
                                        context: context, index: index);
                                  }));
                        }),
                    _dynamicChips(_locationChips),
                    _buildListTile(
                        context: context,
                        title: 'Category_Subcategory',
                        image: 'categories',
                        function: () {
                          setState(() {
                            _categoryChips.clear();
                            //_categoryChipsId.clear();
                          });
                          _showSheet(
                              context,
                              _sheet(
                                  title: 'Category',
                                  length: _categoryList.length,
                                  function: (context, index) {
                                    return _buildCategoryItem(
                                        context: context, index: index);
                                  }));
                        }),
                    _dynamicChips(_categoryChips),
                    SizedBox(height: 20),
                    RoundedLoadingButton(
                      // curve: Curves.ease,
                      //width: screenWidth * .6,
                      color: primaryColor,
                      width: hBlock * 85,
                      controller: _btnController,
                      onPressed: () {
                        locationChips = _locationChips;
                        categoryChips = _categoryChips;
                        print(locationChips.length);

                        _btnController.reset();
                        setState(() {
                          // Navigator.of(context).pop();
                          Navigator.of(context).pushNamed(DivisionsPage.tag);
                        });
                        //currentbarIndex = 1;
                      },

                      child: MyText.mText(
                          context: context, text: 'Done', color: Colors.white),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  Widget _dynamicChips(List<Area> list) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Wrap(
        spacing: 8,
        runSpacing: 8,
        children: List<Widget>.generate(list.length, (int index) {
          return Chip(
            deleteIcon: Icon(Icons.close),
            labelPadding: EdgeInsets.only(left: 6, right: 6),
            padding: EdgeInsets.all(8),
            deleteIconColor: canvusColor,
            backgroundColor: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
            label: Text(
              list[index].name,
              style: TextStyle(
                color: canvusColor,
                fontSize: mFont,
                //fontWeight: FontWeight.bold
              ),
            ),
            onDeleted: () {
              setState(() {
                if (index == 0) {
                  list.clear();
                } else if (index == 1) {
                  if (list.length == 3) {
                    list.removeAt(2);
                    list.removeAt(1);
                  } else {
                    list.removeAt(1);
                  }
                } else {
                  list.removeAt(index);
                }
              });
            },
          );
        }),
      ),
    );
  }

  Widget _buildListTile(
      {BuildContext context, String title, String image, Function function}) {
    return ListTile(
        leading: CustomIcon(
          radius: 25,
          image: image,
          imageSize: 25,
        ),
        title: MyText.mText(
          context: context,
          text: title,
        ),
        trailing: Icon(Icons.arrow_forward_ios),
        onTap: function);
  }

  Widget _sheet({String title, int length, Function function}) {
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom + 10,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(
                      child: Container(
                        height: 5,
                        width: 50,
                        color: accentColor,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 46,
                      ),
                      MyText.lText(context: context, text: title),
                      IconButton(
                        icon: Icon(Icons.close),
                        iconSize: 30,
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    physics: ScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: length,
                    itemBuilder: function,
                  ),
                  // Btn.button(context: context, title: 'Done', onPressed: () {})
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  Widget _buildCountryItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(_countriesList[index].name),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).pop();
              print(_countriesList[index].name);
              setState(() {
                _locationChips.add(_countriesList[index]);
                // _locationChipsId.add(_countriesList[index].id);
                print(_countriesList[index].id);
              });
              _showSheet(
                  context,
                  _sheet(
                      title: 'City',
                      length: _citiesList.length,
                      function: (context, index) {
                        return _buildCityItem(context: context, index: index);
                      }));
            }),
        Container(
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }

  Widget _buildCityItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(_citiesList[index].name),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).pop();
              print(_citiesList[index].id);
              setState(() {
                _locationChips.add(_citiesList[index]);
              });
              // Those are for multiselection trial
              //
              // _districtList.forEach((branch) {
              //   branch.isSelected = false;
              // });
              // _showSheet(context, _distrSheet());
              _showSheet(
                  context,
                  _sheet(
                      title: 'District',
                      length: _districtList.length,
                      function: (context, index) {
                        return _buildDistrictItem(context: context, index: index);
                      }));
            }),
        Container(
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }
Widget _buildDistrictItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(_districtList[index].name),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).pop();
              print(_districtList[index].id);
              setState(() {
                _locationChips.add(_districtList[index]);
              });

              // Those are for multiselection trial
              //
              // _districtList.forEach((branch) {
              //   branch.isSelected = false;
              // });
              // _showSheet(context, _distrSheet());
              // _showSheet(
              //     context,
              //     _sheet(
              //         title: 'District',
              //         length: _citiesList.length,
              //         function: (context, index) {
              //           return _buildDistrictItem(context: context, index: index);
              //         }));
            }),
        Container(
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }

  // Widget _distrSheet() {
  //   return Directionality(
  //     textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
  //     child: StatefulBuilder(
  //         builder: (BuildContext context, StateSetter setState) {
  //       return SingleChildScrollView(
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.end,
  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //           children: <Widget>[
  //             Padding(
  //               padding: const EdgeInsets.all(10.0),
  //               child: Center(
  //                 child: Container(
  //                   height: 5,
  //                   width: 50,
  //                   color: accentColor,
  //                 ),
  //               ),
  //             ),
  //             Padding(
  //               padding: const EdgeInsets.symmetric(vertical: 10),
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 children: <Widget>[
  //                   IconButton(
  //                     icon: Icon(Icons.arrow_back_ios),
  //                     onPressed: () {
  //                       Navigator.of(context).pop();
  //                       // _showSheet(context, _bookingSheet(), true);
  //                     },
  //                   ),
  //                   MyText.lText(context: context, text: 'District'),
  //                   SizedBox(
  //                     width: 46,
  //                   ),
  //                 ],
  //               ),
  //             ),
  //             ListView.builder(
  //               scrollDirection: Axis.vertical,
  //               physics: ScrollPhysics(),
  //               shrinkWrap: true,
  //               itemCount: _districtList.length,
  //               itemBuilder: (context, index) {
  //                 return _buildDistrictItem(
  //                     context: context, index: index, setState: setState);
  //               },
  //             ),
  //           ],
  //         ),
  //       );
  //     }),
  //   );
  // }

  // Widget _buildDistrictItem(
  //     {BuildContext context, int index, StateSetter setState}) {
  //   return Column(
  //     children: <Widget>[
  //       ListTile(
  //         onTap: () {
  //           print(_districtList[index].name);
  //           setState(() {
  //             _districtList[index].isSelected =
  //                 !_districtList[index].isSelected;
  //             if (_districtList[index].isSelected) {
  //               _locationChips.add(_districtList[index]);
  //             } else {
  //               _locationChips.remove(_districtList[index]);
  //             }
  //             print('rrrrrr' + _districtList[index].isSelected.toString());
  //           });
  //         },
  //         title: Text(_districtList[index].name),
  //         trailing: Padding(
  //           padding: const EdgeInsets.all(8.0),
  //           // child: CheckboxListTile(
  //           //     checkColor: canvusColor,
  //           //     activeColor: primaryColor,
  //           //     value: _checked,
  //           //     onChanged: (newValue) {
  //           //       setState(() {
  //           //         _checked = newValue;
  //           //       });
  //           //     }),
  //           child: Container(
  //               color: _districtList[index].isSelected
  //                   ? primaryColor
  //                   : accentColor,
  //               height: 30,
  //               width: 30),
  //           //  Checkbox(
  //           //   checkColor: canvusColor,
  //           //   activeColor: primaryColor,
  //           //   value: _checked, //_districtList[index].isSelected,
  //           //   onChanged: (newValue) {
  //           //     print(newValue);
  //           //     setState(() {
  //           //       _checked = newValue;
  //           //       print(_districtList[index].name);
  //           //       _locationChips.add(_districtList[index].name);
  //           //       _locationChipsId.add(_districtList[index].id);
  //           //     });
  //           //   },
  //           // ),
  //         ),
  //       ),
  //       Container(
  //         height: 1,
  //         width: double.infinity,
  //         color: accentColor,
  //       )
  //     ],
  //   );
  // }

  Widget _buildCategoryItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(_categoryList[index].name),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).pop();
              print(_categoryList[index].id);
              setState(() {
                _categoryChips.add(_categoryList[index]);
                // _categoryChipsId.add(_categoryList[index].id);
              });
              _showSheet(
                  context,
                  _sheet(
                      title: 'Subcategory',
                      length: _subcategoryList.length,
                      function: (context, index) {
                        return _buildSubCategoryItem(
                            context: context, index: index);
                      }));
            }),
        Container(
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }

  Widget _buildSubCategoryItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(_subcategoryList[index].name),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).pop();
              print(_subcategoryList[index].name);
              setState(() {
                _categoryChips.add(_subcategoryList[index]);
                // _categoryChipsId.add(_subcategoryList[index].id);
              });
            }),
        Container(
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }

  _getFiltrationByData() async {
    ApiResponseModel responseModel = await NetworkUtil.getWithoutToken(
      'dataFilter',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      HomeFilterModel model = HomeFilterModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      print('object');
      _countriesList = model.countries;
      _citiesList = model.cities;
      _districtList = model.areas;
      _categoryList = model.categories;
      _subcategoryList = model.subCategories;

      setState(() {
        _isLoading = false;
      });
    } else {
      //Show error alert
      _isLoading = false;
    }
  }
}
