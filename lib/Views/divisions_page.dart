import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:lookin/Generics/NetworkUtil.dart';
import 'package:lookin/Models/all_filtration_model.dart';
import 'package:lookin/Models/api_response_model.dart';
import 'package:lookin/Models/home_model.dart';
import 'package:lookin/Views/Details/details_page.dart';
import 'package:lookin/Widgets/home_photo_widget.dart';
import 'package:lookin/Widgets/text_widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import '../PreDefined/app_constants.dart';
import '../PreDefined/localization.dart';
import '../Utils/size_config.dart';

class DivisionsPage extends StatefulWidget {
  static const tag = '/DivisionsPage';

  @override
  _DivisionsPageState createState() => _DivisionsPageState();
}

class _DivisionsPageState extends State<DivisionsPage> {
  AppLocalizations appLocalizations;
  List<RestaurantsData> _restaurantsData = [];
  bool _isLoading = false;

  @override
  void initState() {
    _getFilteredData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: canvusColor,
          title: MyText.mTextBold(
              context: context,
              text: 'Categories',
              color: Colors.black), 
          centerTitle: true,
          actions: <Widget>[
            IconButton(
                iconSize: 40,
                icon: Icon(
                  MdiIcons.filter,
                  color: primaryColor,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          ],
        ),
        body: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : _restaurantsData.length == 0
                ? Center(
                  child: Container(
                      height: vBlock * 70,
                      width: hBlock * 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            MdiIcons.filter,
                            color: accentColor,
                            size: hBlock * 50,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          MyText.mTextNL(
                              context: context,
                              text: 'No filter data available',
                              color: accentColor)
                        ],
                      ),
                    ),
                )

                // NoDataAvailable(
                //     image: 'assets/filter.png',
                //     text: 'Favourites_are_empty',
                //   )
                : SingleChildScrollView(
                   padding: padding,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        _dynamicChips(locationChips),
                        _dynamicChips(categoryChips),
                        ListView.builder(
                          scrollDirection: Axis.vertical,
                          physics: ScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: _restaurantsData.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetailsPage(
                                          // id: _restaurantsData[index].id,
                                          // title: _restaurantsData[index].name,
                                          restaurantData:
                                              _restaurantsData[index])),
                                );
                              },
                              child: HomePhotoWidget(_restaurantsData[index]),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
      ),
    );
  }

  Widget _dynamicChips(List<Area> list) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Wrap(
        spacing: 8,
        runSpacing: 8,
        children: List<Widget>.generate(list.length, (int index) {
          return Chip(
            //deleteIcon: Icon(Icons.close),
            labelPadding: EdgeInsets.only(left: 6, right: 6),
            padding: EdgeInsets.all(8),
            // deleteIconColor: canvusColor,
            backgroundColor: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
            label: Text(
              list[index].name,
              style: TextStyle(
                color: canvusColor,
                fontSize: xsFont,
                //fontWeight: FontWeight.bold
              ),
            ),
            // onDeleted: () {
            //   // setState(() {
            //   //   if (index == 0) {
            //   //     list.clear();
            //   //   } else if (index == 1) {
            //   //     if (list.length == 3) {
            //   //       list.removeAt(2);
            //   //       list.removeAt(1);
            //   //     } else {
            //   //       list.removeAt(1);
            //   //     }
            //   //   } else {
            //   //     list.removeAt(index);
            //   //   }
            //   //   _getFilteredData();
            //   //   print(locationChips.length);
            //   //   print(categoryChips.length);
            //   // });
            // },
          );
        }),
      ),
    );
  }

  _getFilteredData() async {
    //TODO Sayed why must use all parameters to get data
    print('rrrrrrrrrrrrrrrrr');
    Map<String, dynamic> jsonMap = {
      "category_id": categoryChips.length == 0 ? null : categoryChips[0].id,
      "sub_category_id": categoryChips.length == 1 ? null : categoryChips[1].id,
      "country_id": locationChips.length == 0 ? null : locationChips[0].id,
      "city_id": locationChips.length == 1 ? null : locationChips[1].id,
    };

    String jsonString = json.encode(jsonMap);
    print(jsonString);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'store/filter',
      jsonString,
    );
    print('responseModel.message' + responseModel.statuscode.toString());
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      Restaurants model = Restaurants.fromJson(responseModel.data);

      if (model.data == null) {
        return;
      }

      // filteredRestaurants = _restaurantsData;
      _restaurantsData.forEach((f) {
        print(f.id);
      });
      // _btnController.success();

      if (mounted) {
        setState(() {
          _restaurantsData = model.data;
          print('_restaurantsData.length' + _restaurantsData.length.toString());

          _isLoading = false;
        });
      }
    } else {
      //  _btnController.reset();
      _isLoading = true;
    }
  }
}
