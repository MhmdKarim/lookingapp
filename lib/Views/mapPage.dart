// //import 'dart:convert';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import '../Models/contactModel.dart';

// //import 'package:http/http.dart' as http;

// import 'package:social_media_buttons/social_media_button.dart';

// import '../PreDefined/app_constants.dart';
// import '../Utils/size_config.dart';

// class MapPage extends StatefulWidget {
//   @override
//   _MapPageState createState() => _MapPageState();
// }

// class _MapPageState extends State<MapPage> {
//   // Future<Contacts> contacts;
//   // Future<Contacts> getcontacts() async {
//   //   //NetworkUtil _util = NetworkUtil();
//   //   final response =
//   //       await http.get('http://royalsportingclub.xyz/api/contacts');
//   //   print(response.body);
//   //   if (response.statusCode == 200) {
//   //     // If server returns an OK response, parse the JSON.
//   //     return Contacts.fromJson(json.decode(response.body));
//   //   } else {
//   //     // If that response was not OK, throw an error.
//   //     throw Exception('Failed to load post');
//   //   }
//   // }

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     SizeConfig.init(context);

//     return Scaffold(
//       //appBar: myAppBar("Contact us"),
//       // drawer: appDrawer(context),
//       body: SingleChildScrollView(
//         child: Column(
//           children: <Widget>[
//             FutureBuilder<Contacts>(
//                 // future: getcontacts(),
//                 builder: (context, snapshot) {
//               if (snapshot.hasData) {
//                 Set<Marker> mark = Set();
//                 BitmapDescriptor myIcon;
//                 InfoWindow infoWindow = InfoWindow(title: "Location");
//                 Marker marker = Marker(
//                   draggable: true,
//                   markerId: MarkerId('markers.length.toString()'),
//                   infoWindow: infoWindow,
//                   position: LatLng(37.00, -122.00
//                       // double.parse(snapshot.data.lat),
//                       // double.parse(snapshot.data.lng),
//                       ),
//                   icon: myIcon,
//                 );
//                 mark.add(marker);
//                 BitmapDescriptor.fromAssetImage(
//                   ImageConfiguration(
//                     size: Size(
//                       20,
//                       20,
//                     ),
//                     devicePixelRatio: 0,
//                   ),
//                   'assets/location.png',
//                 ).then((onValue) {
//                   myIcon = onValue;
//                 });
//                 return Column(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   children: <Widget>[
//                     Container(
//                       width: double.infinity,
//                       height: screenHeight * .5,
//                       child: GoogleMap(
//                         buildingsEnabled: true,
//                         initialCameraPosition: CameraPosition(
//                             target: LatLng(37.00, -122.00
//                                 // double.parse(snapshot.data.lat),
//                                 // double.parse(snapshot.data.lng)
//                                 ),
//                             zoom: 15.0),
//                         markers: mark,
//                         //markers: Set.from(),
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Row(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: <Widget>[
//                           Padding(
//                             padding: const EdgeInsets.all(8.0),
//                             child: Text(
//                               "Visit us at : ",
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                           ),
//                           Text(
//                             snapshot.data.addressEn,
//                             style: TextStyle(
//                                 color: Colors.amber,
//                                 fontWeight: FontWeight.bold),
//                           ),
//                         ],
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         children: <Widget>[
//                           Padding(
//                             padding: const EdgeInsets.all(8.0),
//                             child: Text(
//                               "Contact us :",
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                           ),
//                           Container(
//                             decoration: BoxDecoration(
//                                 color: Colors.white12,
//                                 borderRadius: BorderRadius.circular(5)),
//                             alignment: Alignment.topCenter,
//                             width: 200,
//                             child: GridView.count(
//                               shrinkWrap: true,
//                               physics: NeverScrollableScrollPhysics(),
//                               padding: const EdgeInsets.only(bottom: 4),
//                               crossAxisSpacing: 4,
//                               mainAxisSpacing: 4,
//                               crossAxisCount: 3,
//                               children: <Widget>[
//                                 SocialMediaButton.youtube(
//                                   color: Colors.amber,
//                                   size: 30,
//                                   url: snapshot.data.youtube,
//                                 ),
//                                 SocialMediaButton.facebook(
//                                   color: Colors.amber,
//                                   size: 30,
//                                   url: snapshot.data.facebook,
//                                 ),
//                                 SocialMediaButton.twitter(
//                                   color: Colors.amber,
//                                   size: 30,
//                                   // url: "https://${snapshot.data.twitter}",
//                                   url: snapshot.data.twitter,
//                                 ),
//                                 SocialMediaButton.instagram(
//                                   color: Colors.amber,
//                                   size: 30,
//                                   url: snapshot.data.instagram,
//                                 ),
//                                 SocialMediaButton.linkedin(
//                                   color: Colors.amber,
//                                   size: 30,
//                                   url: snapshot.data.linkedin,
//                                 ),
//                               ],
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                   ],
//                 );
//               } else if (snapshot.hasError) {
//                 return Text("${snapshot.error}");
//               }
//               return Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 children: <Widget>[
//                   CircularProgressIndicator(),
//                 ],
//               );
//             }),
//           ],
//         ),
//       ),
//     );
//   }
// }
