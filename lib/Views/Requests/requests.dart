import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lookin/Widgets/button.dart';
import 'package:lookin/Widgets/custom_appbar_widget.dart';
import 'package:lookin/Widgets/no_data_widget.dart';
import '../../Models/bookings_model.dart';
import '../../Generics/NetworkUtil.dart';
import '../../Models/api_response_model.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import '../../Widgets/alert_widget.dart';
import '../../Widgets/text_widgets.dart';

class RequestsPage extends StatefulWidget {
  @override
  _RequestsPageState createState() => _RequestsPageState();
}

class _RequestsPageState extends State<RequestsPage>
    with TickerProviderStateMixin {
  AppLocalizations appLocalizations;
  TabController _tabController;
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  bool _isLoadingAll = true;
  bool _isLoadingAccepted = true;
  bool _isLoadingRejected = true;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<BookingsDataModel> _allBookingList = [];
  List<BookingsDataModel> _acceptedBookingList = [];
  List<BookingsDataModel> _rejectedBookingList = [];
  String requestStatus = '';
  int _stars = 0;
  final _ratingController = TextEditingController();

  bool ratingDone = false;
  bool cancelDone = false;

  int _pageAll = 1;
  int _lastPageAll = 1;

  int _pageAccepted = 1;
  int _lastPageAccepted = 1;

  int _pageRejected = 1;
  int _lastPageRejected = 1;

  @override
  void initState() {
    super.initState();
    _getAllBookings();
    _getAcceptedBookings();
    _getRejectedBookings();
    _tabController = TabController(vsync: this, length: 3);
    _tabController.addListener(_handleTabSelection);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }
    void _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      switch (_tabController.index) {
        case 0:
          print('Page 1 tapped.');
          break;
        case 1:
          print('Page 2 tapped.');
          break;
        case 2:
          print('Page 3 tapped.');
          break;
       
      }
    }
  }

  Future<void> _refreshAllData() async {
    if (_pageAll > _lastPageAll) {
      _isLoadingAll = false;
      return;
    } else {
      await new Future.delayed(new Duration(seconds: 1));

      setState(() {
        print(' I am refreshing data');
        _getAllBookings();
      });
    }
  }

  Future<void> _refreshAcceptedData() async {
    if (_pageAll > _lastPageAccepted) {
      _isLoadingAccepted = false;
      return;
    } else {
      setState(() {
        print(' I am refreshing data');
        _getAcceptedBookings();
      });
    }
  }

  Future<void> _refreshRejectedData() async {
    if (_pageAll > _lastPageRejected) {
      _isLoadingRejected = false;
      return;
    } else {
      setState(() {
        print(' I am refreshing data');
        _getRejectedBookings();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);
    SizeConfig.init(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar.appBar(
          context,
          'Requests',
          TabBar(
            isScrollable: false,
            unselectedLabelColor: Colors.grey,
            indicatorColor: primaryColor,
            labelColor: primaryColor,
            indicatorWeight: 3.0,
            tabs: <Widget>[
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'All',
                ),
              ),
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'The_Confirmed',
                ),
              ),
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'The_rejected',
                ),
              ),
            ],
            controller: _tabController,
          ),
        ),
        body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: <Widget>[
            constUserLoginModel.id == 0
                ? NoDataAvailable(
                    image: 'assets/requests-empty.png',
                    text: 'Sorry_no_requests_available',
                    visible: false,
                  )
                : _isLoadingAll
                    ? Center(child: CircularProgressIndicator())
                    : _allBookingList.length == 0
                        ? NoDataAvailable(
                            image: 'assets/requests-empty.png',
                            text: 'Sorry_no_requests_available',
                            visible: true,
                            onPressed: _refreshAllData,
                          )
                        : LazyLoadScrollView(
                            scrollOffset: 100,
                            onEndOfPage: _refreshAllData,
                            child: RefreshIndicator(
                              onRefresh: _refreshAllData,
                              child: ListView.builder(
                                padding: padding,
                                // controller: _allController,
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _allBookingList.length,
                                itemBuilder: (context, index) {
                                  print(index);
                                  return _buildItems(index, _allBookingList);
                                },
                              ),
                            ),
                          ),
            constUserLoginModel.id == 0
                ? NoDataAvailable(
                    image: 'assets/requests-empty.png',
                    text: 'Sorry_no_requests_available',
                    visible: false,
                  )
                : _isLoadingAccepted
                    ? Center(child: CircularProgressIndicator())
                    : _acceptedBookingList.length == 0
                        ? NoDataAvailable(
                            image: 'assets/requests-empty.png',
                            text: 'Sorry_no_requests_available',
                            visible: true,
                            onPressed: _refreshAcceptedData,
                          )
                        : LazyLoadScrollView(
                            scrollOffset: 0,
                            onEndOfPage: _refreshAcceptedData,
                            child: RefreshIndicator(
                              onRefresh: _refreshAcceptedData,
                              child: ListView.builder(
                                padding: padding,
                                // controller: _acceptedController,
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _acceptedBookingList.length,
                                itemBuilder: (context, index) {
                                  return _buildItems(
                                      index, _acceptedBookingList);
                                },
                              ),
                            ),
                          ),
            constUserLoginModel.id == 0
                ? NoDataAvailable(
                    image: 'assets/requests-empty.png',
                    text: 'Sorry_no_requests_available',
                    visible: false,
                  )
                : _isLoadingRejected
                    ? Center(child: CircularProgressIndicator())
                    : _rejectedBookingList.length == 0
                        ? NoDataAvailable(
                            image: 'assets/requests-empty.png',
                            text: 'Sorry_no_requests_available',
                            visible: true,
                            onPressed: _refreshRejectedData,
                          )
                        : LazyLoadScrollView(
                            scrollOffset: 0,
                            onEndOfPage: _refreshRejectedData,
                            child: RefreshIndicator(
                              onRefresh: _refreshRejectedData,
                              child: ListView.builder(
                                padding: padding,
                                // controller: _rejectedController,
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _rejectedBookingList.length,
                                itemBuilder: (context, index) {
                                  return _buildItems(
                                      index, _rejectedBookingList);
                                },
                              ),
                            ),
                          ),
          ],
        ),
      ),
    );
  }

  Widget _buildItems(int index, List<BookingsDataModel> list) {
    return Container(
      margin:
          EdgeInsets.only(top: hBlock * 2, left: hBlock * 2, right: hBlock * 2),
      // height: 200,
      width: hBlock * 100,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        border: Border.all(
          color: accentColor,
          width: 1,
        ),
      ),
      child: Row(
        children: <Widget>[
          Container(
              width: hBlock * 25,
              child: CachedNetworkImage(
                imageUrl: list[index].restaurant.image,
                fit: BoxFit.cover,
                placeholder: (context, url) =>
                    Center(child: CircularProgressIndicator()),
                errorWidget: (context, url, error) => Padding(
                  padding: EdgeInsets.all(hBlock * 2),
                  child: Image.asset(
                    'assets/logo1.png',
                    fit: BoxFit.fitWidth,
                  ),
                ),
              )),
          Container(
            padding: EdgeInsets.only(
                top: hBlock * 2, left: hBlock * 2, right: hBlock * 2),
            width: hBlock * 60,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: MyText.sTextNLBold(
                      context: context, text: list[index].restaurant.name),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      MyText.sText(
                        context: context,
                        text: 'Booking_Date',
                      ),
                      Text(list[index].date),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      MyText.sText(
                        context: context,
                        text: 'Time',
                      ),
                      Text(list[index].time),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      MyText.sText(
                        context: context,
                        text: 'No_Persons',
                      ),
                      Text(list[index].slotNo.toString()),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      MyText.sText(
                        context: context,
                        text: 'Request_Status',
                      ),
                      Text(
                        list[index].status == 1
                            ? appLocalizations.getMessageByLangAndKey(
                                appLanguageCode, 'Waiting_Confirmation')
                            : list[index].status == 2
                                ? appLocalizations.getMessageByLangAndKey(
                                    appLanguageCode, 'Confirmed')
                                : list[index].status == 3
                                    ? appLocalizations.getMessageByLangAndKey(
                                        appLanguageCode, 'attended')
                                    : list[index].status == 4
                                        ? appLocalizations
                                            .getMessageByLangAndKey(
                                                appLanguageCode,
                                                'Did_not_attend')
                                        : list[index].status == 5
                                            ? appLocalizations
                                                .getMessageByLangAndKey(
                                                    appLanguageCode,
                                                    'client_canceled')
                                            : 'refused',
                        style: TextStyle(
                          color: list[index].status == 1
                              ? Colors.orange
                              : list[index].status == 2
                                  ? primaryColor
                                  : list[index].status == 3
                                      ? primaryColor
                                      : list[index].status == 4
                                          ? Colors.red
                                          : list[index].status == 5
                                              ? Colors.red
                                              : Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
                // list[index].status == 4
                //     ? Container(
                //         height: 0,
                //       )
                //     :
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      list[index].rate != null
                          ? _bottomPartRated(list[index].rate.rate.toInt())
                          : _buildBottom(
                              option: list[index].status,
                              bookingId: list[index].id),
                      Container(
                        height: 1,
                        width: hBlock * 65,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildBottom({int option, int bookingId}) {
    switch (option) {
      case 1: //return ratingDone ? _bottomPartRated() : _bottomPartAddRate(bookingId);
        return cancelDone
            ? BottomPartCancelDone(context: context)
            : _bottomPartCancel(bookingId);
      case 2:
        return cancelDone
            ? BottomPartCancelDone(context: context)
            : _bottomPartCancel(bookingId);
      case 3:
        return ratingDone
            ? _bottomPartRated(_stars)
            : _bottomPartAddRate(bookingId);
      case 4:
        return Container();
      case 5:
        return Container();
      case 6:
        return Container();
    }
  }

  Widget _bottomPartCancel(int id) {
    return Column(
      children: <Widget>[
        Container(
          height: 1,
          width: hBlock * 65,
          color: accentColor,
        ),
        FlatButton(
          onPressed: () {
            print(id);
            MyPopup.showPopUp(
                context: context,
                title: appLocalizations.getMessageByLangAndKey(
                    appLanguageCode, 'confirmation'),
                message: appLocalizations.getMessageByLangAndKey(
                    appLanguageCode, 'Sure_Cancel_reservation?'),
                function: () {
                  _cancelBookings(id);
                });
          },
          child: Text(
            appLocalizations.getMessageByLangAndKey(
                appLanguageCode, 'Cancel_Booking'),
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
    );
  }

  Widget _bottomPartAddRate(int bookingId) {
    return Column(
      children: <Widget>[
        Container(
          height: 1,
          width: hBlock * 65,
          color: accentColor,
        ),
        FlatButton(
            onPressed: () {
              _showRatingPopUp(context, bookingId);
            },
            child: MyText.sText(
                context: context,
                text: 'Rate_your_Experience',
                color: Colors.orange)),
      ],
    );
  }

  Widget _bottomPartRated(int stars) {
    return Column(
      children: <Widget>[
        Container(
          height: 1,
          width: hBlock * 65,
          color: accentColor,
        ),
        Center(
          child: Container(
            padding: EdgeInsets.all(5),
            height: 40,
            child: IgnorePointer(
              child: RatingBar(
                initialRating:
                    stars.toDouble(), // provider.getStars.toDouble(),,
                itemSize: hBlock * 6,
                itemCount: 5,
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: primaryColor,
                ),
                unratedColor: Colors.grey,
                textDirection: TextDirection.rtl,
                allowHalfRating: true,
                onRatingUpdate: (value) {},
              ),
            ),
          ),
        ),
        Container(height: 1)
      ],
    );
  }

  _showRatingPopUp(BuildContext context, int bookingID) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Directionality(
          textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
          child: SimpleDialog(
            contentPadding: MediaQuery.of(context).viewInsets +
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  Text(
                    appLocalizations.getMessageByLangAndKey(
                        appLanguageCode, 'Rate_your_Experience'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  IconButton(
                    iconSize: 30,
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
              ratingStars(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _ratingController,
                  textInputAction: TextInputAction.newline,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration: InputDecoration(
                    labelText: appLocalizations.getMessageByLangAndKey(
                        appLanguageCode, 'Write_your_Rating'),
                    hintText: appLocalizations.getMessageByLangAndKey(
                        appLanguageCode, 'Write_your_Rating'),
                    hintStyle: TextStyle(
                      fontSize: hBlock * 5,
                    ),
                    labelStyle: TextStyle(
                      fontSize: hBlock * 5,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(8),
                width: hBlock * 85,
                height: 40,
                child: RoundedLoadingButton(
                  // curve: Curves.ease,
                  //width: screenWidth * .6,
                  color: primaryColor,
                  width: hBlock * 85,
                  controller: _btnController,
                  onPressed: () {
                    _postRating(bookingID);
                  },
                  child: MyText.sText(
                      context: context,
                      text: 'Add_Your_Rating',
                      color: Colors.white),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget ratingStars() {
    return StatefulBuilder(builder: (BuildContext context, setState) {
      return Center(
        child: RatingBar(
          initialRating: 0, // provider.getStars.toDouble(),,
          itemSize: hBlock * 13,
          itemCount: 5,
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: primaryColor,
          ),
          unratedColor: Colors.grey,
          textDirection: TextDirection.rtl,
          allowHalfRating: true,
          onRatingUpdate: (value) {
            setState(() {
              _stars = value.toInt();
            });
            print(_stars);
          },
        ),
      );
    });
  }

  _getAllBookings() async {
    if (constUserLoginModel == null) {
      return;
    }
    ApiResponseModel responseModel = await NetworkUtil.get(
      'bookings?page=$_pageAll',
      // jsonString,
    );
    if (responseModel == null) {
      return;
    }
    print(responseModel.data);
    if (responseModel.statuscode == 200) {
      BookingsModel model = BookingsModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _lastPageAll = model.lastPage;
      _pageAll++;
      print(responseModel.statuscode);
      _allBookingList.addAll(model.data);

      if (mounted) {
        setState(() {
          _isLoadingAll = false;
        });
      }
    } else {
      _isLoadingAll = false;
    }
  }

  _getAcceptedBookings() async {
    if (constUserLoginModel == null) {
      return;
    }
    ApiResponseModel responseModel = await NetworkUtil.get(
      'bookings/2?page=$_pageAccepted',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      BookingsModel model = BookingsModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _lastPageAccepted = model.lastPage;
      _pageAccepted++;
      print('object');

      _acceptedBookingList.addAll(model.data);
      if (mounted) {
        setState(() {
          _isLoadingAccepted = false;
        });
      }
    } else {
      _isLoadingAccepted = false;
    }
  }

  _getRejectedBookings() async {
    if (constUserLoginModel == null) {
      return;
    }
    ApiResponseModel responseModel = await NetworkUtil.get(
      'bookings/6?page=$_pageRejected',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      BookingsModel model = BookingsModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _lastPageRejected = model.lastPage;
      _pageRejected++;
      _rejectedBookingList.addAll(model.data);
      if (mounted) {
        setState(() {
          _isLoadingRejected = false;
        });
      }
    } else {
      _isLoadingRejected = false;
    }
  }

  _postRating(int bookingId) async {
    Map<String, dynamic> jsonMap = {
      "comment": _ratingController.text,
      "rate": _stars,
      "booking_id": bookingId
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'rates',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      _btnController.success();
      print(responseModel.message);
      setState(() {
        Navigator.of(context).pop();
        ratingDone = true;
      });
    } else {
      _btnController.reset();
    }
  }

  _cancelBookings(int id) async {
    ApiResponseModel responseModel = await NetworkUtil.put(
      'bookings/$id',
    );
    print('bookings/$id');
    if (responseModel == null) {
      return;
    }
    print(responseModel.message);
    if (responseModel.statuscode == 200) {
      Navigator.of(context).pop();
      if (mounted) {
        setState(() {
          cancelDone = true;
        });
      }
    } else {}
  }
}

class BottomPartCancelDone extends StatelessWidget {
  const BottomPartCancelDone({
    Key key,
    @required this.context,
  }) : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: MyText.sText(
              context: context, text: 'Booking_Canceled', color: Colors.red),
        ),
      ),
    );
  }
}
