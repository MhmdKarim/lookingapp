class GenericFunctions {
  static String convertDateToAPI(DateTime selectedDate) {
    return '${selectedDate.month.toString().padLeft(2, '0')}-${selectedDate.day.toString().padLeft(2, '0')}-${selectedDate.year.toString()}';
  }

  static DateTime convertStringToDateTimePicker(String selectedDate) {
    List<String> list = selectedDate.split('-');
    selectedDate = list[2] + '-' + list[0] + '-' + list[1];
    return DateTime.parse(selectedDate);
  }

    static intToDate(int timeInMillis) {
    var datetime = DateTime.fromMillisecondsSinceEpoch(timeInMillis);
    String stringifiedDatetime = datetime.toString();
    var selectedDate = stringifiedDatetime.substring(0, 10);
    return selectedDate;
  }

  static intToTime(int timeInMillis) {
    var datetime = DateTime.fromMillisecondsSinceEpoch(timeInMillis);
    String stringifiedDatetime = datetime.toString();
    var selectedTime = stringifiedDatetime.substring(11, 16);
    return selectedTime;
  }
}
